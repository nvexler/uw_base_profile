#!/bin/sh
rm -rf libraries modules themes
echo Building site profile...
drush make --working-copy --force-gitinfofile --no-recursion --no-core -y --contrib-destination=. uw_base_profile.make
chmod -R g+w *
