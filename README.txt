Prerequisites
-------------

PHP is required; the install profile has been tested with version 5.3.3.

The install profile uses a lot of memory when installing, and the typical default PHP memory limit of 128MB is not enough. A 512 MB limit is recommended to avoid installation issues.

The LDAP module for PHP5 is required for the Contact Feature to retrieve user information from LDAP (php5-ldap). This module may not be installed by default.

A copy of Drupal is also required; this version of the install profile has been tested on Drupal 7.39.

In order to retrieve all the files required for the install, the rebuild.sh script requires the installation of Drush and Git. Drush >= 7.0 but <8 is required.

Finally, a database installation is also required. MySQL is recommended, and the install profile has been tested on 5.5.25.


Installation
------------

To get things started, the profile files should be copied to "drupal/profiles/uw_base_profile".

Following that, run "./rebuild.sh" from the "drupal/profiles/uw_base_profile" folder to pull down all the required themes, modules, features and libraries.

After that is complete, start a Drupal site installation process (http://<your Drupal site address>/install.php).

If the profile is sitting in the correct folder, you should see "uWaterloo Base Profile" in addition to the usual Standard and Minimal installation profiles. Select that and press the "Save and continue" button.

On the next page, "Choose language", select English and continue.

The "Verify requirements" page will only appear if you are not satisfying a requirement, for example by having incorrect folder permissions. You must satisfy all requirements to proceed.

The "Set up database" page appears if your database information was not previously configured. Enter your database information and press "Save and continue".

On the next page you will have 2 options, "quick" or "standard".

Quick Option
------------

If you choose "quick" then the site will be installed from a database file.

It will then run any database updates.

It will then revert any features that are overridden.

Once that is complete, you will be at the "Configure site" page. This is the standard Drupal site configuration page; enter the information and continue. Please note that you are creating a admin local user for this point and not a CAS user. Press "Save and continue".

See before for how to update the database dump file.

Standard Option
---------------

The "Install profile" page will appear as Drupal is installing.

Once that is complete, you will be at the "Configure site" page. This is the standard Drupal site configuration page; enter the information and continue. Please note that you are creating a admin local user for this point and not a CAS user. Press "Save and continue".

Next is the "Enable features" page. Here you can enable the content types you want to use on your site. The web page content type is turned on by default. Additionally, you can enable modules that help with development here. Please do not enable these modules on a production site.

Finally, you can choose to have some basic pages created for you. The last stage can take several minutes to complete, regardless of whether or not you create pages.

HOWTO update the database dump for the Quick option
---------------------------------------------------

This is how to update the database dump file found at db/uw_base_profile.mysql.

1. Checkout the desired version of uw_base_profile.

2. Run rebuild.sh.

3. Check that sites/all and the site's modules and themes directories are empty.

4. Disable installing from database by commenting-out the lines in uw_base_profile.profile that reference install_from_db.

5. Install the site using "drush site-install uw_base_profile".

6. Verify that the roles table has the correct rid for every role. These must match the order that the roles appear in uw_roles_fdsu.

7. Create a dump file using phpMyAdmin (mysqldump does not work). In phpMyAdmin, make the following changes from the default export options:
	Check "Disable foreign key checks".
	Check "Add DROP TABLE / VIEW / PROCEDURE / FUNCTION".
	Uncheck "Use hexadecimal for BLOB".

8. Copy and paste the export data into db/uw_base_profile.mysql.

9. Re-enable installing from database, undoing step 3.

10. Reinstall the site to test that the install works correctly.

11. If it works, commit the new db/uw_base_profile.mysql file.
