<?php

include_once( 'install_from_db/install_from_db.install.inc');

/**
 * This function is called by form submit handlers to increase script execution time limits.
 */
function _uw_base_profile_max_execution_time() {
  // Increase execution time for installs.
  drupal_set_time_limit(0);

  // Make sure the MySQL does not die while scripts are executed.
  if (db_driver() === 'mysql') {
    db_query('SET SESSION wait_timeout = 480');
  }
}

/**
 * Implements hook_form_FORM_ID_alter().
 *
 * Allows the profile to alter the site configuration form.
 */
function uw_base_profile_form_install_configure_form_alter(&$form, $form_state) {
  // Pre-populate the site name with the server name.
  $form['site_information']['site_name']['#default_value'] = $_SERVER['SERVER_NAME'];
  // Pre-populate the country with "Canada".
  $form['server_settings']['site_default_country']['#default_value'] = 'CA';
  // Disable time zone autodetect and default to "America/Toronto".
  $form['server_settings']['date_default_timezone']['#attributes']['class'][0] = '';
  $form['server_settings']['date_default_timezone']['#default_value'] = 'America/Toronto';
  // Disable email notifications by default.
  $form['update_notifications']['update_status_module']['#default_value'][1] = 0;
}

/**
 * Implements hook_install_tasks().
 */
function uw_base_profile_install_tasks(&$install_state) {
  $tasks = array();

  $tasks['uw_base_profile_install_site_controller'] = array(
    'display_name' => st('Site controller'),
    'type' => 'normal',
  );
  $tasks['uw_select_site_features_form'] = array(
    'display_name' => st('Features'),
    'type' => 'form',
  );
  $tasks['uw_create_default_content_form'] = array(
    'display_name' => st('Create content'),
    'type' => 'form',
  );
  $tasks['uw_install_uw_auth_wcms_admins'] = array(
    'display_name' => st('Set up administrators'),
    'type' => 'normal',
  );
  /*
  // Revert all features.
  $tasks['uw_base_profile_features_revert_all'] = array(
    'type' => 'normal',
  );
  // Set default permissions.
  $tasks['uw_base_profile_set_permissions'] = array(
    'type' => 'normal',
  );
  // Rebuild node access.
  $tasks['uw_base_profile_node_access_rebuid'] = array(
    'type' => 'normal',
  );
  */
  // Skip tasks of installing site controller and all interactive form if using quickstart.
  if (!empty($install_state['parameters']['quickstart']) && $install_state['parameters']['quickstart'] == 'quick') {
    foreach ($tasks as $task_name => $task) {
     $tasks['uw_base_profile_install_site_controller']['run'] = INSTALL_TASK_SKIP;
     if (isset($tasks[$task_name]['type']) && $tasks[$task_name]['type'] == 'form') {
        $tasks[$task_name]['run'] = INSTALL_TASK_SKIP;
     }
    }
  }
  return $tasks;
}

/**
 * Implements hook_install_tasks_alter()
 */
function uw_base_profile_install_tasks_alter(&$tasks, $install_state) {
  install_from_db_install_tasks_alter($tasks, $install_state);
}

/**
 * Task callback: installs the site controller.
  */
function uw_base_profile_install_site_controller() {
  $success = module_enable(array('uw_site_fdsu'));
  if (!$success) {
    return 'Could not enable site controller, a module dependency was missing -- the function module_enable() returned FALSE';
  }
}

/**
 * Revert all features.
 */
function uw_base_profile_features_revert_all() {
  global $install_state;
  drupal_set_time_limit(0);
  // Revert specific feature sections before reverting them all.
  //features_revert(array(
  //  'feature_name' => array('feature_section1', 'feature_section2'),
  //));
  features_revert();
}

/**
 * Wrapper around node access rebuild, as didn't work till I wrapped it.
 */
function uw_base_profile_node_access_rebuild() {
  node_access_rebuild();
  _uw_base_profile_remove_message('Content permissions have been rebuilt');
}

/**
 * Task callback: returns the form allowing the user to turn on features at install time.
 * Made the content types and dev. modules checked by default (#default_value => TRUE)
 * to speed up site creation time for local development (Dec, 15 2012).
 */
function uw_select_site_features_form() {
  drupal_set_title(st('Enable features'));

  $form['set_auth_type'] = array(
    '#type' => 'select',
    '#title' => st('Select the authentication type:'),
    '#options' => array(
      0 => st('Closed: do not create Drupal accounts for any CAS login'),
      1 => st('Open: create Drupal accounts for any CAS login')
    ),
    '#default_value' => 0,
  );
  $form['enable_content_types'] = array(
    '#type' => 'fieldset',
    '#title' => st('Enable content types'),
    '#collapsible' => FALSE,
    '#tree' => TRUE,
  );
  // the array key is the name of the module
  $form['enable_content_types']['uw_ct_award'] = array(
    '#type' => 'checkbox',
    '#title' => st('Award'),
    '#default_value' => 0,
  );
  $form['enable_content_types']['uw_ct_bibliography'] = array(
    '#type' => 'checkbox',
    '#title' => st('Bibliography'),
    '#default_value' => 1,
  );
  $form['enable_content_types']['uw_ct_blog'] = array(
    '#type' => 'checkbox',
    '#title' => st('Blog'),
    '#default_value' => 1,
  );
  $form['enable_content_types']['uw_ct_contact'] = array(
    '#type' => 'checkbox',
    '#title' => st('Contacts'),
    '#default_value' => 1,
  );
  $form['enable_content_types']['uw_ct_custom_listing'] = array(
    '#type' => 'checkbox',
    '#title' => st('Custom listing page'),
    '#default_value' => 1,
  );
  $form['enable_content_types']['uw_ct_embedded_timeline'] = array(
    '#type' => 'checkbox',
    '#title' => st('Embedded timeline'),
    '#default_value' => 1,
  );
  $form['enable_content_types']['uw_ct_event'] = array(
    '#type' => 'checkbox',
    '#title' => st('Events'),
    '#default_value' => 1,
  );
  $form['enable_content_types']['uw_ct_home_page_banner'] = array(
    '#type' => 'checkbox',
    '#title' => st('Home page banners'),
    '#default_value' => 1,
  );
  $form['enable_content_types']['uw_ct_image_gallery'] = array(
    '#type' => 'checkbox',
    '#title' => st('Image galleries'),
    '#default_value' => 1,
  );
  $form['enable_content_types']['uw_ct_news_item'] = array(
    '#type' => 'checkbox',
    '#title' => st('News items'),
    '#default_value' => 1,
  );
  $form['enable_content_types']['uw_ct_opportunities'] = array(
    '#type' => 'checkbox',
    '#title' => st('Opportunities'),
    '#default_value' => 0,
  );
  $form['enable_content_types']['uw_ct_person_profile'] = array(
    '#type' => 'checkbox',
    '#title' => st('People profiles'),
    '#default_value' => 1,
  );
  $form['enable_content_types']['uw_ct_project'] = array(
    '#type' => 'checkbox',
    '#title' => st('Project'),
    '#default_value' => 1,
  );
  $form['enable_content_types']['uw_ct_promo_item'] = array(
    '#type' => 'checkbox',
    '#title' => st('Promotional items'),
    '#default_value' => 1,
  );
  $form['enable_content_types']['uw_ct_service'] = array(
    '#type' => 'checkbox',
    '#title' => st('Service'),
    '#default_value' => 1,
  );
  $form['enable_content_types']['uw_ct_special_alert'] = array(
    '#type' => 'checkbox',
    '#title' => st('Special Alert'),
    '#default_value' => 1,
  );
  $form['enable_content_types']['uw_ct_web_form'] = array(
    '#type' => 'checkbox',
    '#title' => st('Web Form'),
    '#default_value' => 1,
  );
  $form['enable_content_types']['uw_captcha'] = array(
    '#type' => 'checkbox',
    '#title' => st('CAPTCHA'),
    '#default_value' => 1,
  );

  /* DEVELOPMENT */
  $form['enable_development'] = array(
    '#type' => 'fieldset',
    '#title' => st('Enable development modules'),
    '#collapsible' => FALSE,
    '#tree' => TRUE,
  );
  $form['enable_development']['context_ui'] = array(
    '#type' => 'checkbox',
    '#title' => st('Context UI'),
    '#default_value' => 1,
  );
  $form['enable_development']['uw_cfg_devel'] = array(
    '#type' => 'checkbox',
    '#title' => st('Devel'),
    '#default_value' => 0,
  );
  $form['enable_development']['devel_generate'] = array(
    '#type' => 'checkbox',
    '#title' => st('Devel Generate'),
    '#default_value' => 0,
    '#states' => array(
      'visible' => array(   // action to take.
        ':input[name="enable_development[devel]"]' // element to evaluate condition on
          => array('checked' => TRUE),  // condition
      ),
    ),
  );
  $form['enable_development']['drupal_reset'] = array(
    '#type' => 'checkbox',
    '#title' => st('Drupal Reset'),
    '#default_value' => 0,
  );
  $form['enable_development']['module_filter'] = array(
    '#type' => 'checkbox',
    '#title' => st('Module filter'),
    '#default_value' => 1,
  );
  $form['enable_development']['views_ui'] = array(
    '#type' => 'checkbox',
    '#title' => st('Views UI'),
    '#default_value' => 1,
  );


  $form['actions'] = array('#type' => 'actions');
  $form['submit'] = array('#type' => 'submit', '#value' => st('Enable and continue'));

  return $form;
}

/**
 * Submit callback: creates the requested default content.
 */
function uw_select_site_features_form_submit(&$form, &$form_state) {
  _uw_base_profile_max_execution_time();

  $modules = array();

  if ($form_state['values']['set_auth_type'] == 1) {
    variable_set('cas_user_register', '1');
  } else {
    variable_set('cas_user_register', '0');
  }

  if (!empty($form_state['values']['enable_content_types'])) {
    $modules = array_merge($modules, $form_state['values']['enable_content_types']);
  }

  if (!empty($form_state['values']['enable_development'])) {
    $modules = array_merge($modules, $form_state['values']['enable_development']);
  }

  if (!empty($modules)) {
    _uw_base_profile_enable_modules($modules);
  }
}

/**
 * Task callback: returns the form allowing the user to create initial or default content on install.
 */
function uw_create_default_content_form() {
  drupal_set_title(st('Create initial default content'));

  if (module_exists('locale')) {

    // Currently there is only English.  This may change.
    $language_options = array(
     'en' => 'English',
     'und' => 'Undefined',
    );
    $form['install_profile_language'] = array(
      '#type' => 'fieldset',
      '#title' => st('Site content language (interface will be English)'),
      '#collapsible' => FALSE,
    );

    $form['install_profile_language']['langcode'] = array(
     '#type' => 'select',
     '#title' => st('Language'),
     '#options' => $language_options,
     '#default_value' => 'en',
     '#description' => t('Use English, unless you know what you are doing.'),
    );
  }
  /* Default Web Pages */
  if (module_exists('uw_ct_web_page')) {

    $form['add_default_pages'] = array(
      '#type' => 'fieldset',
      '#title' => st('Create default web pages'),
      '#collapsible' => FALSE,
      '#tree' => TRUE,
    );

    $form['add_default_pages']['home'] = array(
      '#type' => 'checkbox',
      '#title' => st('Home'),
      '#default_value' => 1,
    );

    $form['add_default_pages']['about'] = array(
      '#type' => 'checkbox',
      '#title' => st('About'),
      '#default_value' => 1,
    );

  }

/* Turn on term lock for profile and audience taxonomies */
    if (module_exists('uw_vocab_audience') || module_exists('uw_ct_person_profile')) {
        $form['add_vocab_pages'] = array(
        '#type' => 'fieldset',
        '#title' => st('Add terms to vocabularies'),
        '#collapsible' => FALSE,
        '#tree' => TRUE,
        );
        /* Add Audience vocabulary terms */
        if (module_exists('uw_vocab_audience')) {
            $form['add_vocab_pages']['audience'] = array(
             '#type' => 'checkbox',
             '#title' => st('Audience Vocabulary Terms'),
             '#default_value' => 1,
             );
        }
        /* Add Profile vocabulary terms */
        if (module_exists('uw_ct_person_profile')) {
             $form['add_vocab_pages']['profile'] = array(
              '#type' => 'checkbox',
              '#title' => st('Profile Vocabulary Terms'),
              '#default_value' => 1,
              );
        }
    }

  $form['actions'] = array('#type' => 'actions');
  $form['submit'] = array('#type' => 'submit', '#value' => st('Continue'));

  return $form;
}

/**
 * Task callback: installs the uw_auth_wcms_admins module.
 */
function uw_install_uw_auth_wcms_admins() {
  _uw_base_profile_enable_modules(array('uw_auth_wcms_admins' => 1));
}

/**
 * Submit callback: creates the requested default content.
 */
function uw_create_default_content_form_submit(&$form, &$form_state) {
  _uw_base_profile_max_execution_time();

  // clear all caches to rebuild the registry and node types that were setup earlier
  drupal_flush_all_caches();
 // rebuild permissions for latest workbench_moderation module (April 28, 2015)
  node_access_rebuild();

  $modules = array();

  // add default pages
  if (!empty($form_state['values']['add_default_pages'])) {
    // put the default pages into a Drupal variable. When the module is enabled, it will use this variable
    // to determine which pages to add
    variable_set('uw_data_web_pages_setup', $form_state['values']['add_default_pages']);
    $modules['uw_data_web_pages'] = 1;

  }
  if (!empty($form_state['values']['add_vocab_pages']['audience'])) {
   $modules['uw_data_audience_categories'] = 1;
  }
  if (!empty($form_state['values']['add_vocab_pages']['profile'])) {
   $modules['uw_data_profile_categories'] = 1;
  }
  if (!empty($form_state['values']['add_vocab_pages']['awarddegree'])) {
   $modules['uw_data_academic_degree'] = 1;
  }
  if (!empty($form_state['values']['add_vocab_pages']['awardcitizenship'])) {
   $modules['uw_data_citizenship'] = 1;
  }
  if (!empty($form_state['values']['add_vocab_pages']['awarddepartment'])) {
   $modules['uw_data_department'] = 1;
  }
  if (!empty($form_state['values']['add_vocab_pages']['awardcategory'])) {
   $modules['uw_data_award_categories'] = 1;
  }
  if (!empty($form_state['values']['add_vocab_pages']['awardterm'])) {
   $modules['uw_data_award_term'] = 1;
  }
  // add default location / writer tips
  if (!empty($form_state['values']['add_default_content'])) {
    $modules = array_merge($modules, $form_state['values']['add_default_content']);
  }
  if (!empty($modules)) {
    _uw_base_profile_enable_modules($modules);
  }
  // No longer need profile language variable.

   // Update all content to the selected Language
   // Jan 16th 2015, Chose not to rewrite all data modules to use selected language.  Instead rewrite
   // it all, as we did in uw_site_fdsu_update_7116() update hook.  This may need to be changed if the profile
   // starts to support languages other than english in inital install.
   if (!empty($form_state['values']['langcode'])) {
      $langcode = check_plain($form_state['values']['langcode']);
      _uw_base_profile_set_content_language($langcode);
  }
}

/**
 * Given an array where key => module_name and value => TRUE/FALSE,
 * enables those modules
 */
function _uw_base_profile_enable_modules($modules = array()) {
  $module_list = array();
  foreach ($modules as $key => $value) {
    if ($value) {
      $module_list[] = $key;
    }
  }
  if (!empty($module_list)) {
    module_enable($module_list);
  }
}

function _uw_base_profile_set_content_language($langcode) {

  $install_languages = language_list();

  // Add English language if it does not exist (Probably do not need to check).
  if (!isset($install_languages['en'])) {
     locale_add_language('en');
     drupal_set_message('Adding English language.');
  }
  // Set default language.
  variable_set('language_default', $install_languages[$langcode]);

  // NOTE: In enabled the FDSU site controller it sets the admin interface language to English.

  // Grab all translatable fields
  $fields = db_select('field_config', 'fc')
    ->fields('fc')
    ->condition('translatable', 1, '=')
    ->execute();
  // Update each translatable field and its revisions
  foreach($fields as $field) {
    db_update('field_data_' . $field->field_name)
      ->fields(array('language' => $langcode,))
      ->condition('language', 'und')
      ->execute();
    db_update('field_revision_' . $field->field_name)
      ->fields(array('language' => $langcode,))
      ->condition('language', 'und')
      ->execute();
  }
  // Update all nodes
  db_update('node')
    ->fields(array('language' => $langcode,))
    ->condition('language', 'und')
    ->execute();
    // Update all url alias
  db_update('url_alias')
    ->fields(array('language' => $langcode,))
    ->condition('language', 'und')
    ->execute();
  // Update all comments
  db_update('comment')
    ->fields(array('language' => $langcode,))
    ->condition('language', 'und')
    ->execute();
  db_update('entity_translation')
    ->fields(array('language' => $langcode,))
    ->condition('language', 'und')
    ->execute();
  db_update('entity_translation_revision')
    ->fields(array('language' => $langcode,))
    ->condition('language', 'und')
    ->execute();
}

/**
 * Remove a message as set by drupal_set_message().
 *
 * This is used during install to remove irrelavent messages.
 */
function _uw_base_profile_remove_message($partial_message, $type = 'status') {
  if (!empty($_SESSION['messages'][$type])) {
    foreach ($_SESSION['messages'][$type] as $key => $message) {
      if (strpos($message, $partial_message) !== FALSE) {
        unset($_SESSION['messages'][$type][$key]);
      }
    }
    if (empty($_SESSION['messages'][$type])) {
      unset($_SESSION['messages'][$type]);
    }
  }
}
