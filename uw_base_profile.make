; uWaterloo Base Profile Make File

core = 7.x
api = 2

projects[] = drupal

;------------------------------------------------------------------------------------------------------------------------
; Contrib Modules
; Contributed modules from Drupal.org
;------------------------------------------------------------------------------------------------------------------------

; Administration Language
projects[admin_language][type] = "module"
projects[admin_language][download][type] = "git"
projects[admin_language][download][url] = "https://git.uwaterloo.ca/drupal-org/admin_language.git"
projects[admin_language][download][tag] = "7.x-1.0-beta1"
projects[admin_language][subdir] = "contrib"

; Better Exposed Filters
projects[better_exposed_filters][type] = "module"
projects[better_exposed_filters][download][type] = "git"
projects[better_exposed_filters][download][url] = "https://git.uwaterloo.ca/drupal-org/better_exposed_filters.git"
projects[better_exposed_filters][download][tag] = "7.x-3.2"
projects[better_exposed_filters][subdir] = "contrib"

; Better Formats
projects[better_formats][type] = "module"
projects[better_formats][download][type] = "git"
projects[better_formats][download][url] = "https://git.uwaterloo.ca/drupal-org/better_formats.git"
projects[better_formats][download][branch] = "7.x-1.x"
projects[better_formats][download][revision] = "3b4a8c9"
projects[better_formats][subdir] = "contrib"

; Biblio
; Base: 7.x-1.0-rc7
; Patch b8536b7: Remove pass-by-reference error on publication pages. https://drupal.org/comment/8694483#comment-8694483
projects[biblio][type] = "module"
projects[biblio][download][type] = "git"
projects[biblio][download][url] = "https://git.uwaterloo.ca/drupal-org/biblio.git"
projects[biblio][download][tag] = "7.x-1.0-rc7-uw_wcms"
projects[biblio][subdir] = "contrib"

; Calendar
projects[calendar][type] = "module"
projects[calendar][download][type] = "git"
projects[calendar][download][url] = "https://git.uwaterloo.ca/drupal-org/calendar.git"
projects[calendar][download][tag] = "7.x-3.5"
projects[calendar][subdir] = "contrib"

; CAPTCHA
projects[captcha][type] = "module"
projects[captcha][download][type] = "git"
projects[captcha][download][url] = "https://git.uwaterloo.ca/drupal-org/captcha.git"
projects[captcha][download][tag] = "7.x-1.3"
projects[captcha][subdir] = "contrib"

; CAS
projects[cas][type] = "module"
projects[cas][download][type] = "git"
projects[cas][download][url] = "https://git.uwaterloo.ca/drupal-org/cas.git"
projects[cas][download][tag] = "7.x-1.4"
projects[cas][subdir] = "contrib"

; CKEditor Link
projects[ckeditor_link][type] = "module"
projects[ckeditor_link][download][type] = "git"
projects[ckeditor_link][download][url] = "https://git.uwaterloo.ca/drupal-org/ckeditor_link.git"
projects[ckeditor_link][download][tag] = "7.x-2.3"
projects[ckeditor_link][subdir] = "contrib"

; Clientside Validation
projects[clientside_validation][type] = "module"
projects[clientside_validation][download][type] = "git"
projects[clientside_validation][download][url] = "https://git.uwaterloo.ca/drupal-org/clientside_validation.git"
projects[clientside_validation][download][tag] = "7.x-1.41"
projects[clientside_validation][subdir] = "contrib"

; Composite Views Filter
projects[composite_views_filter][type] = "module"
projects[composite_views_filter][download][type] = "git"
projects[composite_views_filter][download][url] = "https://git.uwaterloo.ca/drupal-org/composite_views_filter.git"
projects[composite_views_filter][download][tag] = "7.x-1.3"
projects[composite_views_filter][subdir] = "contrib"

; Conditional Fields
; Base: 78ecb04 (7.x-3.0-alpha1+12-dev)
; Patch efd3840: Fix undefined index error on feature enable. https://www.drupal.org/node/2043563#comment-9049737
; Patch 743e60d: Fix conditional fields not saving. https://www.drupal.org/node/1542706#comment-9585345
projects[conditional_fields][type] = "module"
projects[conditional_fields][download][type] = "git"
projects[conditional_fields][download][url] = "https://git.uwaterloo.ca/drupal-org/conditional_fields.git"
projects[conditional_fields][download][tag] = "7.x-3.0-alpha1+12-dev-uw_wcms2"
projects[conditional_fields][subdir] = "contrib"

; Context
projects[context][type] = "module"
projects[context][download][type] = "git"
projects[context][download][url] = "https://git.uwaterloo.ca/drupal-org/context.git"
projects[context][download][tag] = "7.x-3.6"
projects[context][subdir] = "contrib"

; Context: Variable
projects[context_var][type] = "module"
projects[context_var][download][type] = "git"
projects[context_var][download][url] = "https://git.uwaterloo.ca/drupal-org/context_var.git"
projects[context_var][download][tag] = "7.x-1.1"
projects[context_var][subdir] = "contrib"

; Chaos tools
projects[ctools][type] = "module"
projects[ctools][download][type] = "git"
projects[ctools][download][url] = "https://git.uwaterloo.ca/drupal-org/ctools.git"
projects[ctools][download][tag] = "7.x-1.9"
projects[ctools][subdir] = "contrib"

; Date
; Base: 7.x-2.9
; Patch 0e0aedb: Issue #998076: Problem with timezone handling (caused by date_get_timezone_db returning only UTC). https://www.drupal.org/node/998076#comment-10055136
; Patch 001f36f: Issue #998076 / RT#459341: date_update_7200() is not running. Applied interdiff: https://www.drupal.org/node/998076#comment-10385751
; Patch bce973d: Issue #2503807 / RT#460609: Nav header appears at wrong spot. https://www.drupal.org/node/2503807#comment-10010257
projects[date][type] = "module"
projects[date][download][type] = "git"
projects[date][download][url] = "https://git.uwaterloo.ca/drupal-org/date.git"
projects[date][download][tag] = "7.x-2.9-uw_wcms3"
projects[date][subdir] = "contrib"

; Date iCal
projects[date_ical][type] = "module"
projects[date_ical][download][type] = "git"
projects[date_ical][download][url] = "https://git.uwaterloo.ca/drupal-org/date_ical.git"
projects[date_ical][download][tag] = "7.x-3.5"
projects[date_ical][subdir] = "contrib"

; Default Menu Link
projects[default_menu_link][type] = "module"
projects[default_menu_link][download][type] = "git"
projects[default_menu_link][download][url] = "https://git.uwaterloo.ca/drupal-org/default_menu_link.git"
projects[default_menu_link][download][tag] = "7.x-1.3"
projects[default_menu_link][subdir] = "contrib"

; Diff
projects[diff][type] = "module"
projects[diff][download][type] = "git"
projects[diff][download][url] = "https://git.uwaterloo.ca/drupal-org/diff.git"
projects[diff][download][tag] = "7.x-3.2"
projects[diff][subdir] = "contrib"

; Disable messages
projects[disable_messages][type] = "module"
projects[disable_messages][download][type] = "git"
projects[disable_messages][download][url] = "https://git.uwaterloo.ca/drupal-org/disable_messages.git"
projects[disable_messages][download][tag] = "7.x-1.1"
projects[disable_messages][subdir] = "contrib"

; Disable Term Node Listings
; Base: 7.x-1.2
; Patch c15ade3: Move menu alter hook to last position, so it executes after other modules (and therefore always works). https://drupal.org/node/1936304#comment-8652541
projects[disable_term_node_listings][type] = "module"
projects[disable_term_node_listings][download][type] = "git"
projects[disable_term_node_listings][download][url] = "https://git.uwaterloo.ca/drupal-org/disable_term_node_listings.git"
projects[disable_term_node_listings][download][tag] = "7.x-1.2-uw_wcms"
projects[disable_term_node_listings][subdir] = "contrib"

; Draggableviews
projects[draggableviews][type] = "module"
projects[draggableviews][download][type] = "git"
projects[draggableviews][download][url] = "https://git.uwaterloo.ca/drupal-org/draggableviews.git"
projects[draggableviews][download][tag] = "7.x-2.1"
projects[draggableviews][subdir] = "contrib"

; EIM
; Base: 7.x-1.3
; Patch 2f90652: Avoid multiple error message boxes. https://drupal.org/comment/8220183#comment-8220183
; Patch 3e362a7: Disable insert module integration / required validation. No Drupal issue.
; Patch 12e8b2b: Error when other modules try to dynamically load fields. https://www.drupal.org/node/2180035#comment-9443645
projects[eim][type] = "module"
projects[eim][download][type] = "git"
projects[eim][download][url] = "https://git.uwaterloo.ca/drupal-org/eim.git"
projects[eim][download][tag] = "7.x-1.3-uw_wcms2"
projects[eim][subdir] = "contrib"

; Elysia Cron
; Base: 7.x-2.1
; Patch b3709b9: RT#382203: Allow the module to work in an install profile. No Drupal issue.
projects[elysia_cron][type] = "module"
projects[elysia_cron][download][type] = "git"
projects[elysia_cron][download][url] = "https://git.uwaterloo.ca/drupal-org/elysia_cron.git"
projects[elysia_cron][download][tag] = "7.x-2.1-uw_wcms"
projects[elysia_cron][subdir] = "contrib"

; Email
projects[email][type] = "module"
projects[email][download][type] = "git"
projects[email][download][url] = "https://git.uwaterloo.ca/drupal-org/email.git"
projects[email][download][tag] = "7.x-1.3"
projects[email][subdir] = "contrib"

; Entity API
projects[entity][type] = "module"
projects[entity][download][type] = "git"
projects[entity][download][url] = "https://git.uwaterloo.ca/drupal-org/entity.git"
projects[entity][download][tag] = "7.x-1.6"
projects[entity][subdir] = "contrib"

; Entity cache [Publication]
projects[entitycache][type] = "module"
projects[entitycache][download][type] = "git"
projects[entitycache][download][url] = "https://git.uwaterloo.ca/drupal-org/entitycache.git"
projects[entitycache][download][tag] = "7.x-1.2"
projects[entitycache][subdir] = "contrib"

; Entity menu links [Publication]
; Base: 7.x-1.0-alpha3
; Patch 9b8514f: Prevent fatal error with RESTFul module menu links. see https://www.drupal.org/node/2222467
projects[entity_menu_links][type] = "module"
projects[entity_menu_links][download][type] = "git"
projects[entity_menu_links][download][url] = "https://git.uwaterloo.ca/drupal-org/entity_menu_links.git"
projects[entity_menu_links][download][tag] = "7.x-1.0-alpha3-uw_wcms1"
projects[entity_menu_links][subdir] = "contrib"

; Entity Reference
; Base: 7.x-1.1
; Patch c828d18: Prevent call to undefined function entityreference_get_behavior_handlers(). https://drupal.org/comment/7271012#comment-7271012
projects[entityreference][type] = "module"
projects[entityreference][download][type] = "git"
projects[entityreference][download][url] = "https://git.uwaterloo.ca/drupal-org/entityreference.git"
projects[entityreference][download][tag] = "7.x-1.1-uw_wcms"
projects[entityreference][subdir] = "contrib"

; Entity Translation
projects[entity_translation][type] = "module"
projects[entity_translation][download][type] = "git"
projects[entity_translation][download][url] = "https://git.uwaterloo.ca/drupal-org/entity_translation.git"
projects[entity_translation][download][tag] = "7.x-1.0-beta4"
projects[entity_translation][subdir] = "contrib"

; Expire
projects[expire][type] = "module"
projects[expire][download][type] = "git"
projects[expire][download][url] = "https://git.uwaterloo.ca/drupal-org/expire.git"
projects[expire][download][tag] = "7.x-2.0-rc4"
projects[expire][subdir] = "contrib"

; Features
projects[features][type] = "module"
projects[features][download][type] = "git"
projects[features][download][url] = "https://git.uwaterloo.ca/drupal-org/features.git"
projects[features][download][tag] = "7.x-2.5"
projects[features][subdir] = "contrib"

; Features Override
projects[features_override][type] = "module"
projects[features_override][download][type] = "git"
projects[features_override][download][url] = "https://git.uwaterloo.ca/drupal-org/features_override.git"
projects[features_override][download][tag] = "7.x-2.0-rc2"
projects[features_override][subdir] = "contrib"

; Feeds
; Base: 7.x-2.0-beta1
; Patch cfb555a: Issue #2531706 by twistor: relation "cache_feeds_http" does not exist. Created by: git cherry-pick e21dbe1
projects[feeds][type] = "module"
projects[feeds][download][type] = "git"
projects[feeds][download][url] = "https://git.uwaterloo.ca/drupal-org/feeds.git"
projects[feeds][download][tag] = "7.x-2.0-beta1-uw_wcms1"
projects[feeds][subdir] = "contrib"

; Feeds XPath Parser
projects[feeds_xpathparser][type] = "module"
projects[feeds_xpathparser][download][type] = "git"
projects[feeds_xpathparser][download][url] = "https://git.uwaterloo.ca/drupal-org/feeds_xpathparser.git"
projects[feeds_xpathparser][download][tag] = "7.x-1.1"
projects[feeds_xpathparser][subdir] = "contrib"

; Field Collection
projects[field_collection][type] = "module"
projects[field_collection][download][type] = "git"
projects[field_collection][download][url] = "https://git.uwaterloo.ca/drupal-org/field_collection.git"
projects[field_collection][download][tag] = "7.x-1.0-beta8"
projects[field_collection][subdir] = "contrib"

; Field Slideshow
projects[field_slideshow][type] = "module"
projects[field_slideshow][download][type] = "git"
projects[field_slideshow][download][url] = "https://git.uwaterloo.ca/drupal-org/field_slideshow.git"
projects[field_slideshow][download][tag] = "7.x-1.82"
projects[field_slideshow][subdir] = "contrib"

; Fieldgroup
projects[field_group][type] = "module"
projects[field_group][download][type] = "git"
projects[field_group][download][url] = "https://git.uwaterloo.ca/drupal-org/field_group.git"
projects[field_group][download][tag] = "7.x-1.4"
projects[field_group][subdir] = "contrib"

; Field Permissions
projects[field_permissions][type] = "module"
projects[field_permissions][download][type] = "git"
projects[field_permissions][download][url] = "https://git.uwaterloo.ca/drupal-org/field_permissions.git"
projects[field_permissions][download][tag] = "7.x-1.0-beta2"
projects[field_permissions][subdir] = "contrib"

; Filefield Sources
projects[filefield_sources][type] = "module"
projects[filefield_sources][download][type] = "git"
projects[filefield_sources][download][url] = "https://git.uwaterloo.ca/drupal-org/filefield_sources.git"
projects[filefield_sources][download][tag] = "7.x-1.10"
projects[filefield_sources][subdir] = "contrib"

; Fill PDF
projects[fillpdf][type] = "module"
projects[fillpdf][download][type] = "git"
projects[fillpdf][download][url] = "https://git.uwaterloo.ca/drupal-org/fillpdf.git"
projects[fillpdf][download][tag] = "7.x-1.10"
projects[fillpdf][subdir] = "contrib"

; Focal Point
; Base: 6d36945 (7.x-1.0-beta4+4-dev)
; Patch 1363913: Added settings per fields vs. enabled for all image fields. Related to: https://www.drupal.org/node/2480947
projects[focal_point][type] = "module"
projects[focal_point][download][type] = "git"
projects[focal_point][download][url] = "https://git.uwaterloo.ca/drupal-org/focal_point.git"
projects[focal_point][download][tag] = "7.x-1.0-beta4-uw_wcms1"
projects[focal_point][subdir] = "contrib"

; Forward
projects[forward][type] = "module"
projects[forward][download][type] = "git"
projects[forward][download][url] = "https://git.uwaterloo.ca/drupal-org/forward.git"
projects[forward][download][tag] = "7.x-2.0"
projects[forward][subdir] = "contrib"

; Gallery Formatter
projects[galleryformatter][type] = "module"
projects[galleryformatter][download][type] = "git"
projects[galleryformatter][download][url] = "https://git.uwaterloo.ca/drupal-org/galleryformatter.git"
projects[galleryformatter][download][tag] = "7.x-1.3"
projects[galleryformatter][subdir] = "contrib"

; GMap
; Base: 7.x-2.10
; Patch 4a148f7: Make pop-ups work. https://www.drupal.org/node/2445429#comment-10314603
projects[gmap][type] = "module"
projects[gmap][download][type] = "git"
projects[gmap][download][url] = "https://git.uwaterloo.ca/drupal-org/gmap.git"
projects[gmap][download][tag] = "7.x-2.10-uw_wcms1"
projects[gmap][subdir] = "contrib"

; Hidden CAPTCHA
projects[hidden_captcha][type] = "module"
projects[hidden_captcha][download][type] = "git"
projects[hidden_captcha][download][url] = "https://git.uwaterloo.ca/drupal-org/hidden_captcha.git"
projects[hidden_captcha][download][branch] = "7.x-1.x"
projects[hidden_captcha][download][revision] = "3461c96"
projects[hidden_captcha][subdir] = "contrib"

; Image Field Caption
projects[image_field_caption][type] = "module"
projects[image_field_caption][download][type] = "git"
projects[image_field_caption][download][url] = "https://git.uwaterloo.ca/drupal-org/image_field_caption.git"
projects[image_field_caption][download][tag] = "7.x-2.1"
projects[image_field_caption][subdir] = "contrib"

; Image Resize Filter
projects[image_resize_filter][type] = "module"
projects[image_resize_filter][download][type] = "git"
projects[image_resize_filter][download][url] = "https://git.uwaterloo.ca/drupal-org/image_resize_filter.git"
projects[image_resize_filter][download][tag] = "7.x-1.16"
projects[image_resize_filter][subdir] = "contrib"

; IMCE
projects[imce][type] = "module"
projects[imce][download][type] = "git"
projects[imce][download][url] = "https://git.uwaterloo.ca/drupal-org/imce.git"
projects[imce][download][tag] = "7.x-1.9"
projects[imce][subdir] = "contrib"

; Insert
projects[insert][type] = "module"
projects[insert][download][type] = "git"
projects[insert][download][url] = "https://git.uwaterloo.ca/drupal-org/insert.git"
projects[insert][download][tag] = "7.x-1.3"
projects[insert][subdir] = "contrib"

; Internationalization (i18n)
projects[i18n][type] = "module"
projects[i18n][download][type] = "git"
projects[i18n][download][url] = "https://git.uwaterloo.ca/drupal-org/i18n.git"
projects[i18n][download][tag] = "7.x-1.13"
projects[i18n][subdir] = "contrib"

; Job Scheduler
projects[job_scheduler][type] = "module"
projects[job_scheduler][download][type] = "git"
projects[job_scheduler][download][url] = "https://git.uwaterloo.ca/drupal-org/job_scheduler.git"
projects[job_scheduler][download][tag] = "7.x-2.0-alpha3"
projects[job_scheduler][subdir] = "contrib"

; Jquery Colorpicker
projects[jquery_colorpicker][type] = "module"
projects[jquery_colorpicker][download][type] = "git"
projects[jquery_colorpicker][download][url] = "https://git.uwaterloo.ca/drupal-org/jquery_colorpicker.git"
projects[jquery_colorpicker][download][tag] = "7.x-1.1"
projects[jquery_colorpicker][subdir] = "contrib"

; jQuery Update
projects[jquery_update][type] = "module"
projects[jquery_update][download][type] = "git"
projects[jquery_update][download][url] = "https://git.uwaterloo.ca/drupal-org/jquery_update.git"
projects[jquery_update][download][tag] = "7.x-2.6"
projects[jquery_update][subdir] = "contrib"

; Label Help
projects[label_help][type] = "module"
projects[label_help][download][type] = "git"
projects[label_help][download][url] = "https://git.uwaterloo.ca/drupal-org/label_help.git"
projects[label_help][download][tag] = "7.x-1.2"
projects[label_help][subdir] = "contrib"

; Libraries
projects[libraries][type] = "module"
projects[libraries][download][type] = "git"
projects[libraries][download][url] = "https://git.uwaterloo.ca/drupal-org/libraries.git"
projects[libraries][download][tag] = "7.x-2.2"
projects[libraries][subdir] = "contrib"

; Link
projects[link][type] = "module"
projects[link][download][type] = "git"
projects[link][download][url] = "https://git.uwaterloo.ca/drupal-org/link.git"
projects[link][download][tag] = "7.x-1.3"
projects[link][subdir] = "contrib"

; Linkchecker
projects[linkchecker][type] = "module"
projects[linkchecker][download][type] = "git"
projects[linkchecker][download][url] = "https://git.uwaterloo.ca/drupal-org/linkchecker.git"
projects[linkchecker][download][tag] = "7.x-1.2"
projects[linkchecker][subdir] = "contrib"

; Location
projects[location][type] = "module"
projects[location][download][type] = "git"
projects[location][download][url] = "https://git.uwaterloo.ca/drupal-org/location.git"
projects[location][download][tag] = "7.x-3.7"
projects[location][subdir] = "contrib"

; Location Feeds
; Base: 7.x-1.6
; Patch 508b1f1: Issue #2367135: Fix inclusion of test to use correct filename from location module. https://www.drupal.org/node/2367135#comment-9569629
projects[location_feeds][type] = "module"
projects[location_feeds][download][type] = "git"
projects[location_feeds][download][url] = "https://git.uwaterloo.ca/drupal-org/location_feeds.git"
projects[location_feeds][download][tag] = "7.x-1.6-uw_wcms1"
projects[location_feeds][subdir] = "contrib"

; mailsystem
projects[mailsystem][type] = "module"
projects[mailsystem][download][type] = "git"
projects[mailsystem][download][url] = "https://git.uwaterloo.ca/drupal-org/mailsystem.git"
projects[mailsystem][download][tag] = "7.x-2.34"
projects[mailsystem][subdir] = "contrib"

; Markup
projects[markup][type] = "module"
projects[markup][download][type] = "git"
projects[markup][download][url] = "https://git.uwaterloo.ca/drupal-org/markup.git"
projects[markup][download][tag] = "7.x-1.2"
projects[markup][subdir] = "contrib"

; MathJax
projects[mathjax][type] = "module"
projects[mathjax][download][type] = "git"
projects[mathjax][download][url] = "https://git.uwaterloo.ca/drupal-org/mathjax.git"
projects[mathjax][download][tag] = "7.x-2.4"
projects[mathjax][subdir] = "contrib"

; Maxlength
projects[maxlength][type] = "module"
projects[maxlength][download][type] = "git"
projects[maxlength][download][url] = "https://git.uwaterloo.ca/drupal-org/maxlength.git"
projects[maxlength][download][tag] = "7.x-3.2"
projects[maxlength][subdir] = "contrib"

; More Buttons
; Base: cf380cf (7.x-1.0-beta1+5-dev)
; Patch 8963d49: Prevent notice: Trying to get property of non-object in mb_content_menu_local_tasks_alter(). https://drupal.org/node/1526178
; Patch a1754dd: Issue #2577143: Fix Notice: Undefined index: #id in mb_content_changed_validate(). https://www.drupal.org/node/2577143#comment-10390005
projects[mb][type] = "module"
projects[mb][download][type] = "git"
projects[mb][download][url] = "https://git.uwaterloo.ca/drupal-org/mb.git"
projects[mb][download][tag] = "7.x-1.0-beta1+5-dev-uw_wcms2"
projects[mb][subdir] = "contrib"

; Memcache
projects[memcache][type] = "module"
projects[memcache][download][type] = "git"
projects[memcache][download][url] = "https://git.uwaterloo.ca/drupal-org/memcache.git"
projects[memcache][download][tag] = "7.x-1.5"
projects[memcache][subdir] = "contrib"

; Menu Admin per Menu
projects[menu_admin_per_menu][type] = "module"
projects[menu_admin_per_menu][download][type] = "git"
projects[menu_admin_per_menu][download][url] = "https://git.uwaterloo.ca/drupal-org/menu_admin_per_menu.git"
projects[menu_admin_per_menu][download][tag] = "7.x-1.1"
projects[menu_admin_per_menu][subdir] = "contrib"

; Metatag
projects[metatag][type] = "module"
projects[metatag][download][type] = "git"
projects[metatag][download][url] = "https://git.uwaterloo.ca/drupal-org/metatag.git"
projects[metatag][download][tag] = "7.x-1.7"
projects[metatag][subdir] = "contrib"

; mimemail
projects[mimemail][type] = "module"
projects[mimemail][download][type] = "git"
projects[mimemail][download][url] = "https://git.uwaterloo.ca/drupal-org/mimemail.git"
projects[mimemail][download][tag] = "7.x-1.0-beta4"
projects[mimemail][subdir] = "contrib"

; NavBar
projects[navbar][type] = "module"
projects[navbar][download][type] = "git"
projects[navbar][download][url] = "https://git.uwaterloo.ca/drupal-org/navbar.git"
projects[navbar][download][tag] = "7.x-1.6"
projects[navbar][subdir] = "contrib"

; 404 Navigation
projects[navigation404][type] = "module"
projects[navigation404][download][type] = "git"
projects[navigation404][download][url] = "https://git.uwaterloo.ca/drupal-org/navigation404.git"
projects[navigation404][download][tag] = "7.x-1.0"
projects[navigation404][subdir] = "contrib"

; Node clone
; Base: 7.x-1.0-rc2
; Patch 6a51792: Issue #2460445: RT#411800: Make 'clone of' work with title module. https://www.drupal.org/node/2460445#comment-9764243
projects[node_clone][type] = "module"
projects[node_clone][download][type] = "git"
projects[node_clone][download][url] = "https://git.uwaterloo.ca/drupal-org/node_clone.git"
projects[node_clone][download][tag] = "7.x-1.0-rc2-uw_wcms1"
projects[node_clone][subdir] = "contrib"

; Node clone tab
projects[node_clone_tab][type] = "module"
projects[node_clone_tab][download][type] = "git"
projects[node_clone_tab][download][url] = "https://git.uwaterloo.ca/drupal-org/node_clone_tab.git"
projects[node_clone_tab][download][tag] = "7.x-1.1"
projects[node_clone_tab][subdir] = "contrib"

; Node export
projects[node_export][type] = "module"
projects[node_export][download][type] = "git"
projects[node_export][download][url] = "https://git.uwaterloo.ca/drupal-org/node_export.git"
projects[node_export][download][tag] = "7.x-3.0"
projects[node_export][subdir] = "contrib"

; Node revision delete
projects[node_revision_delete][type] = "module"
projects[node_revision_delete][download][type] = "git"
projects[node_revision_delete][download][url] = "https://git.uwaterloo.ca/drupal-org/node_revision_delete.git"
projects[node_revision_delete][download][tag] = "7.x-2.6"
projects[node_revision_delete][subdir] = "contrib"

; Office hours
projects[office_hours][type] = "module"
projects[office_hours][download][type] = "git"
projects[office_hours][download][url] = "https://git.uwaterloo.ca/drupal-org/office_hours.git"
projects[office_hours][download][tag] = "7.x-1.4"
projects[office_hours][subdir] = "contrib"

; Options element
projects[options_element][type] = "module"
projects[options_element][download][type] = "git"
projects[options_element][download][url] = "https://git.uwaterloo.ca/drupal-org/options_element.git"
projects[options_element][download][tag] = "7.x-1.12"
projects[options_element][subdir] = "contrib"

; Override node options
projects[override_node_options][type] = "module"
projects[override_node_options][download][type] = "git"
projects[override_node_options][download][url] = "https://git.uwaterloo.ca/drupal-org/override_node_options.git"
projects[override_node_options][download][tag] = "7.x-1.13"
projects[override_node_options][subdir] = "contrib"

; Page Title
; Base: 7.x-2.7
; Patch 631c02a: Ensure Characters Entered text is not duplicated on node add. https://drupal.org/comment/4822042#comment-4822042
projects[page_title][type] = "module"
projects[page_title][download][type] = "git"
projects[page_title][download][url] = "https://git.uwaterloo.ca/drupal-org/page_title.git"
projects[page_title][download][tag] = "7.x-2.7-uw_wcms"
projects[page_title][subdir] = "contrib"

; Paragraphs
projects[paragraphs][type] = "module"
projects[paragraphs][download][type] = "git"
projects[paragraphs][download][url] = "https://git.uwaterloo.ca/drupal-org/paragraphs.git"
projects[paragraphs][download][branch] = "7.x-1.x"
projects[paragraphs][download][revision] = "135809e1"
projects[paragraphs][subdir] = "contrib"

; Pathauto
projects[pathauto][type] = "module"
projects[pathauto][download][type] = "git"
projects[pathauto][download][url] = "https://git.uwaterloo.ca/drupal-org/pathauto.git"
projects[pathauto][download][tag] = "7.x-1.2"
projects[pathauto][subdir] = "contrib"

; Pathauto persistant state
projects[pathauto_persist][type] = "module"
projects[pathauto_persist][download][type] = "git"
projects[pathauto_persist][download][url] = "https://git.uwaterloo.ca/drupal-org/pathauto_persist.git"
projects[pathauto_persist][download][tag] = "7.x-1.3"
projects[pathauto_persist][subdir] = "contrib"

; Postal Code Validation
projects[postal_code_validation][type] = "module"
projects[postal_code_validation][download][type] = "git"
projects[postal_code_validation][download][url] = "https://git.uwaterloo.ca/drupal-org/postal_code_validation.git"
projects[postal_code_validation][download][tag] = "7.x-1.4"
projects[postal_code_validation][subdir] = "contrib"

; Quicktabs
projects[quicktabs][type] = "module"
projects[quicktabs][download][type] = "git"
projects[quicktabs][download][url] = "https://git.uwaterloo.ca/drupal-org/quicktabs.git"
projects[quicktabs][download][tag] = "7.x-3.6"
projects[quicktabs][subdir] = "contrib"

; Real name
projects[realname][type] = "module"
projects[realname][download][type] = "git"
projects[realname][download][url] = "https://git.uwaterloo.ca/drupal-org/realname.git"
projects[realname][download][tag] = "7.x-1.2"
projects[realname][subdir] = "contrib"

; reCAPTCHA
projects[recaptcha][type] = "module"
projects[recaptcha][download][type] = "git"
projects[recaptcha][download][url] = "https://git.uwaterloo.ca/drupal-org/recaptcha.git"
projects[recaptcha][download][tag] = "7.x-1.12"
projects[recaptcha][subdir] = "contrib"

; Redirect
; Base: 7.x-1.0-rc3
; Patch 8f71fac: Issue #2327247: RT#220517: Validate internal URLs in redirects and allow <front> as a destination. https://www.drupal.org/node/2327247#comment-9894229
projects[redirect][type] = "module"
projects[redirect][download][type] = "git"
projects[redirect][download][url] = "https://git.uwaterloo.ca/drupal-org/redirect.git"
projects[redirect][download][tag] = "7.x-1.0-rc3-uw_wcms1"
projects[redirect][subdir] = "contrib"

; References
projects[references][type] = "module"
projects[references][download][type] = "git"
projects[references][download][url] = "https://git.uwaterloo.ca/drupal-org/references.git"
projects[references][download][tag] = "7.x-2.1"
projects[references][subdir] = "contrib"

; Responsive Tables Filter
; Base: 128442e (7.x-1.1+3-dev)
; Patch 033068f: Only make tables responsive on sites running uw_fdsu_theme_resp. No Drupal issue.
projects[responsive_tables_filter][type] = "module"
projects[responsive_tables_filter][download][type] = "git"
projects[responsive_tables_filter][download][url] = "https://git.uwaterloo.ca/drupal-org/responsive_tables_filter.git"
projects[responsive_tables_filter][download][tag] = "7.x-1.1+3-dev-uw_wcms1"
projects[responsive_tables_filter][subdir] = "contrib"

; Restful
; Base: 7.x-1.3
; Patch e339287: Return null for empty or unavailable fields. See: https://github.com/navigationarts/restful/commit/3e05042e85a2b2aebefed74dda6819e49f8f9926
projects[restful][type] = "module"
projects[restful][download][type] = "git"
projects[restful][download][url] = "https://git.uwaterloo.ca/drupal-org/restful.git"
projects[restful][download][tag] = "7.x-1.3-uw_wcms1"
projects[restful][subdir] = "contrib"

; Reuse Cached 404s
projects[reuse_cached_404][type] = "module"
projects[reuse_cached_404][download][type] = "git"
projects[reuse_cached_404][download][url] = "https://git.uwaterloo.ca/drupal-org/reuse_cached_404.git"
projects[reuse_cached_404][download][tag] = "7.x-1.0"
projects[reuse_cached_404][subdir] = "contrib"

; Role Expire
; Base: 7.x-1.0-beta2
; Patch c4b5461: Role Assign integration. https://drupal.org/comment/8706305#comment-8706305
projects[role_expire][type] = "module"
projects[role_expire][download][type] = "git"
projects[role_expire][download][url] = "https://git.uwaterloo.ca/drupal-org/role_expire.git"
projects[role_expire][download][tag] = "7.x-1.0-beta2-uw_wcms1"
projects[role_expire][subdir] = "contrib"

; RoleAssign
projects[roleassign][type] = "module"
projects[roleassign][download][type] = "git"
projects[roleassign][download][url] = "https://git.uwaterloo.ca/drupal-org/roleassign.git"
projects[roleassign][download][tag] = "7.x-1.0"
projects[roleassign][subdir] = "contrib"

; Rules
projects[rules][type] = "module"
projects[rules][download][type] = "git"
projects[rules][download][url] = "https://git.uwaterloo.ca/drupal-org/rules.git"
projects[rules][download][tag] = "7.x-2.9"
projects[rules][subdir] = "contrib"

; Scheduler
; Base: dab5c09 (7.x-1.3+2-dev)
; Patch 09280ee: Patch Scheduler module for publish and unpublish specific user. https://www.drupal.org/node/2397423#comment-9483701
; Patch 4f562e4: Fixed error on line 59 of scheduler.edit.inc.
projects[scheduler][type] = "module"
projects[scheduler][download][type] = "git"
projects[scheduler][download][url] = "https://git.uwaterloo.ca/drupal-org/scheduler.git"
projects[scheduler][download][tag] = "7.x-1.3+2-dev-uw_wcms2"
projects[scheduler][subdir] = "contrib"

; Scheduler Workbench Integration
; Base: 7.x-1.2
; Patch 8fcad74: Issue #2230539: Fixed Scheduler does not unplubish when using workbench moderation. https://www.drupal.org/node/2230539#comment-9458491
; Patch 59675e2: Issue #2230539: Apply interdiff to update to patch version in comment #14. https://www.drupal.org/node/2230539#comment-9553805
; Patch 0db85a0: FDSU-563. Added uw_wb_config dependency to allow setting of moderation permissions. No Drupal issue.
projects[scheduler_workbench][type] = "module"
projects[scheduler_workbench][download][type] = "git"
projects[scheduler_workbench][download][url] = "https://git.uwaterloo.ca/drupal-org/scheduler_workbench.git"
projects[scheduler_workbench][download][tag] = "7.x-1.2-uw_wcms3"
projects[scheduler_workbench][subdir] = "contrib"

; Schema.org
projects[schemaorg][type] = "module"
projects[schemaorg][download][type] = "git"
projects[schemaorg][download][url] = "https://git.uwaterloo.ca/drupal-org/schemaorg.git"
projects[schemaorg][download][tag] = "7.x-1.0-rc1"
projects[schemaorg][subdir] = "contrib"

; Search Configuration
projects[search_config][type] = "module"
projects[search_config][download][type] = "git"
projects[search_config][download][url] = "https://git.uwaterloo.ca/drupal-org/search_config.git"
projects[search_config][download][tag] = "7.x-1.1"
projects[search_config][subdir] = "contrib"

; Select (or other)
projects[select_or_other][type] = "module"
projects[select_or_other][download][type] = "git"
projects[select_or_other][download][url] = "https://git.uwaterloo.ca/drupal-org/select_or_other.git"
projects[select_or_other][download][tag] = "7.x-2.22"
projects[select_or_other][subdir] = "contrib"

; Service links
; Base: 7.x-2.3
; Patch 2dc7f37: Improve HTML of service links in blocks. https://www.drupal.org/node/2072891#comment-7886391
projects[service_links][type] = "module"
projects[service_links][download][type] = "git"
projects[service_links][download][url] = "https://git.uwaterloo.ca/drupal-org/service_links.git"
projects[service_links][download][tag] = "7.x-2.3-uw_wcms1"
projects[service_links][subdir] = "contrib"

; Services
projects[services][type] = "module"
projects[services][download][type] = "git"
projects[services][download][url] = "https://git.uwaterloo.ca/drupal-org/services_.git"
projects[services][download][tag] = "7.x-3.12"
projects[services][subdir] = "contrib"

; Services API Key Authentication
projects[services_api_key_auth][type] = "module"
projects[services_api_key_auth][download][type] = "git"
projects[services_api_key_auth][download][url] = "https://git.uwaterloo.ca/drupal-org/services_api_key_auth.git"
projects[services_api_key_auth][download][tag] = "7.x-1.0"
projects[services_api_key_auth][subdir] = "contrib"

; Services Views
projects[services_views][type] = "module"
projects[services_views][download][type] = "git"
projects[services_views][download][url] = "https://git.uwaterloo.ca/drupal-org/services_views.git"
projects[services_views][download][tag] = "7.x-1.1"
projects[services_views][subdir] = "contrib"

; Special Menu Items
projects[special_menu_items][type] = "module"
projects[special_menu_items][download][type] = "git"
projects[special_menu_items][download][url] = "https://git.uwaterloo.ca/drupal-org/special_menu_items.git"
projects[special_menu_items][download][tag] = "7.x-2.0"
projects[special_menu_items][subdir] = "contrib"

; Strongarm
projects[strongarm][type] = "module"
projects[strongarm][download][type] = "git"
projects[strongarm][download][url] = "https://git.uwaterloo.ca/drupal-org/strongarm.git"
projects[strongarm][download][tag] = "7.x-2.0"
projects[strongarm][subdir] = "contrib"

; Tablefield
projects[tablefield][type] = "module"
projects[tablefield][download][type] = "git"
projects[tablefield][download][url] = "https://git.uwaterloo.ca/drupal-org/tablefield.git"
projects[tablefield][download][tag] = "7.x-2.4"
projects[tablefield][subdir] = "contrib"

; Tab Tamer
projects[tabtamer][type] = "module"
projects[tabtamer][download][type] = "git"
projects[tabtamer][download][url] = "https://git.uwaterloo.ca/drupal-org/tabtamer.git"
projects[tabtamer][download][tag] = "7.x-1.1"
projects[tabtamer][subdir] = "contrib"

; Taxonomy access fix
projects[taxonomy_access_fix][type] = "module"
projects[taxonomy_access_fix][download][type] = "git"
projects[taxonomy_access_fix][download][url] = "https://git.uwaterloo.ca/drupal-org/taxonomy_access_fix.git"
projects[taxonomy_access_fix][download][tag] = "7.x-2.2"
projects[taxonomy_access_fix][subdir] = "contrib"

; Taxonomy display
projects[taxonomy_display][type] = "module"
projects[taxonomy_display][download][type] = "git"
projects[taxonomy_display][download][url] = "https://git.uwaterloo.ca/drupal-org/taxonomy_display.git"
projects[taxonomy_display][download][tag] = "7.x-1.1"
projects[taxonomy_display][subdir] = "contrib"

; Taxonomy Formatter
projects[taxonomy_formatter][type] = "module"
projects[taxonomy_formatter][download][type] = "git"
projects[taxonomy_formatter][download][url] = "https://git.uwaterloo.ca/drupal-org/taxonomy_formatter.git"
projects[taxonomy_formatter][download][tag] = "7.x-1.4"
projects[taxonomy_formatter][subdir] = "contrib"

; Taxonomy Machine Name [Publication]
projects[taxonomy_machine_name][type] = "module"
projects[taxonomy_machine_name][download][type] = "git"
projects[taxonomy_machine_name][download][url] = "https://git.uwaterloo.ca/drupal-org/taxonomy_machine_name.git"
projects[taxonomy_machine_name][download][tag] = "7.x-1.0"
projects[taxonomy_machine_name][subdir] = "contrib"

; Taxonomy menu form [Publication]
; Patch 51eb8f6: Custom UWaterloo patch to use unique permission instead of "administer menu"
projects[taxonomy_menu_form][type] = "module"
projects[taxonomy_menu_form][download][type] = "git"
projects[taxonomy_menu_form][download][url] = "https://git.uwaterloo.ca/drupal-org/taxonomy_menu_form.git"
projects[taxonomy_menu_form][download][tag] = "7.x-1.1-uw_wcms1"
projects[taxonomy_menu_form][subdir] = "contrib"

; Taxonomy Orphanage
projects[taxonomy_orphanage][type] = "module"
projects[taxonomy_orphanage][download][type] = "git"
projects[taxonomy_orphanage][download][url] = "https://git.uwaterloo.ca/drupal-org/taxonomy_orphanage.git"
projects[taxonomy_orphanage][download][tag] = "7.x-1.1"
projects[taxonomy_orphanage][subdir] = "contrib"

; Term Reference Tree
; Base: 7.x-1.10
; Patch 40b9050: Use current weights and terms in display formatter. https://drupal.org/comment/7330974#comment-7330974
; Patch 6b04704: Provide "use inline display" option. No Drupal issue.
; Patch a986226: Removed bold styling introduced in latest version. No Drupal issue.
; Patch 81676fb: i18n compatibility. https://www.drupal.org/node/1514794#comment-7533593
projects[term_reference_tree][type] = "module"
projects[term_reference_tree][download][type] = "git"
projects[term_reference_tree][download][url] = "https://git.uwaterloo.ca/drupal-org/term_reference_tree.git"
projects[term_reference_tree][download][tag] = "7.x-1.10-uw_wcms2"
projects[term_reference_tree][subdir] = "contrib"

; Title
projects[title][type] = "module"
projects[title][download][type] = "git"
projects[title][download][url] = "https://git.uwaterloo.ca/drupal-org/title.git"
projects[title][download][branch] = "7.x-1.x"
projects[title][download][revision] = "d6f2000"
projects[title][subdir] = "contrib"

; Token
projects[token][type] = "module"
projects[token][download][type] = "git"
projects[token][download][url] = "https://git.uwaterloo.ca/drupal-org/token.git"
projects[token][download][tag] = "7.x-1.6"
projects[token][subdir] = "contrib"

; Transliteration
projects[transliteration][type] = "module"
projects[transliteration][download][type] = "git"
projects[transliteration][download][url] = "https://git.uwaterloo.ca/drupal-org/transliteration.git"
projects[transliteration][download][tag] = "7.x-3.2"
projects[transliteration][subdir] = "contrib"

; User protect
; Base: 7.x-1.1
; Patch 3643474: Prevent users from being able to cancel their own accounts when protected. https://drupal.org/comment/7293406#comment-7293406
projects[userprotect][type] = "module"
projects[userprotect][download][type] = "git"
projects[userprotect][download][url] = "https://git.uwaterloo.ca/drupal-org/userprotect.git"
projects[userprotect][download][tag] = "7.x-1.1-uw_wcms1"
projects[userprotect][subdir] = "contrib"

; UUID
; Base: 7.x-1.0-alpha6
; Patch 6f581b7: Fix upgrade failure. https://www.drupal.org/node/2248581#comment-9429451
projects[uuid][type] = "module"
projects[uuid][download][type] = "git"
projects[uuid][download][url] = "https://git.uwaterloo.ca/drupal-org/uuid.git"
projects[uuid][download][tag] = "7.x-1.0-alpha6-uw_wcms1"
projects[uuid][subdir] = "contrib"

; UUID Features
; Base: 2e5217a (7.x-1.0-alpha4+75-dev)
; Patch c41c204: Issue #2158057: UUID support for default value in taxonomy term reference field settings. https://www.drupal.org/node/2158057#comment-10404641
projects[uuid_features][type] = "module"
projects[uuid_features][download][type] = "git"
projects[uuid_features][download][url] = "https://git.uwaterloo.ca/drupal-org/uuid_features.git"
projects[uuid_features][download][tag] = "7.x-1.0-alpha4+75-dev-uw_wcms1"
projects[uuid_features][subdir] = "contrib"

; Varnish
projects[varnish][type] = "module"
projects[varnish][download][type] = "git"
projects[varnish][download][url] = "https://git.uwaterloo.ca/drupal-org/varnish.git"
projects[varnish][download][branch] = "7.x-1.x"
projects[varnish][download][revision] = "211afb2"
projects[varnish][subdir] = "contrib"

; Views
; Base: 7.x-3.11
; Patch 5b99f58: Allow dynamic expose filters. https://www.drupal.org/node/1183418
projects[views][type] = "module"
projects[views][download][type] = "git"
projects[views][download][url] = "https://git.uwaterloo.ca/drupal-org/views.git"
projects[views][download][tag] = "7.x-3.11-uw_wcms1"
projects[views][subdir] = "contrib"

; Views Bulk Operations
projects[views_bulk_operations][type] = "module"
projects[views_bulk_operations][download][type] = "git"
projects[views_bulk_operations][download][url] = "https://git.uwaterloo.ca/drupal-org/views_bulk_operations.git"
projects[views_bulk_operations][download][tag] = "7.x-3.3"
projects[views_bulk_operations][subdir] = "contrib"

; Views Data Export
projects[views_data_export][type] = "module"
projects[views_data_export][download][type] = "git"
projects[views_data_export][download][url] = "https://git.uwaterloo.ca/drupal-org/views_data_export.git"
projects[views_data_export][download][tag] = "7.x-3.0-beta8"
projects[views_data_export][subdir] = "contrib"

; Views Field View
projects[views_field_view][type] = "module"
projects[views_field_view][download][type] = "git"
projects[views_field_view][download][url] = "https://git.uwaterloo.ca/drupal-org/views_field_view.git"
projects[views_field_view][download][tag] = "7.x-1.1"
projects[views_field_view][subdir] = "contrib"

; Views iCal
projects[views_ical][type] = "module"
projects[views_ical][download][type] = "git"
projects[views_ical][download][url] = "https://git.uwaterloo.ca/drupal-org/views_ical.git"
projects[views_ical][download][tag] = "7.x-1.0-beta2"
projects[views_ical][subdir] = "contrib"

; Views PHP
; Base: 7.x-1.0-alpha1
; Patch c8afb1a: Issue #2276165: Correct issue with pagination on events page. https://www.drupal.org/node/2276165
projects[views_php][type] = "module"
projects[views_php][download][type] = "git"
projects[views_php][download][url] = "https://git.uwaterloo.ca/drupal-org/views_php.git"
projects[views_php][download][tag] = "7.x-1.0-alpha1-uw_wcms1"
projects[views_php][subdir] = "contrib"

; Views Raw SQL
; Base: 7.x-1.0-rc1
; Patch eeae32f: Support proper featurization. https://drupal.org/node/2002288#comment-7446408
projects[views_raw_sql][type] = "module"
projects[views_raw_sql][download][type] = "git"
projects[views_raw_sql][download][url] = "https://git.uwaterloo.ca/drupal-org/views_raw_sql.git"
projects[views_raw_sql][download][tag] = "7.x-1.0-rc1-uw_wcms1"
projects[views_raw_sql][subdir] = "contrib"

; Views Timeline JS integration
; Base: c0bde8c (7.x-1.0-beta1+1-dev)
; Patch bbc1d90: Issue #2549435: Fix undefined indexes when creating feature. https://www.drupal.org/node/2549435#comment-10208149
; Patch 380c29f: Issue #2602910: Fix undefined index notices. https://www.drupal.org/node/2602910#comment-10497960
; Patch 8ae29fa: Issue #2602880: Support alt text for images. https://www.drupal.org/node/2602880#comment-10497834
projects[views_timelinejs][type] = "module"
projects[views_timelinejs][download][type] = "git"
projects[views_timelinejs][download][url] = "https://git.uwaterloo.ca/drupal-org/views_timelinejs.git"
projects[views_timelinejs][download][tag] = "7.x-1.0-beta1+1-dev-uw_wcms2"
projects[views_timelinejs][subdir] = "contrib"

; Webform
projects[webform][type] = "module"
projects[webform][download][type] = "git"
projects[webform][download][url] = "https://git.uwaterloo.ca/drupal-org/webform.git"
projects[webform][download][branch] = "7.x-4.x"
projects[webform][download][revision] = "5eaadea"
projects[webform][subdir] = "contrib"

; Webform Validation
projects[webform_validation][type] = "module"
projects[webform_validation][download][type] = "git"
projects[webform_validation][download][url] = "https://git.uwaterloo.ca/drupal-org/webform_validation.git"
projects[webform_validation][download][tag] = "7.x-1.10"
projects[webform_validation][subdir] = "contrib"

; Workbench
; Base: 7.x-1.2
; Patch bd77ef7e: RT#393703: Remove contextual link error with Patch 13 of https://www.drupal.org/node/1727284.
projects[workbench][type] = "module"
projects[workbench][download][type] = "git"
projects[workbench][download][url] = "https://git.uwaterloo.ca/drupal-org/workbench.git"
projects[workbench][download][tag] = "7.x-1.2-uw_wcms1"
projects[workbench][subdir] = "contrib"

; Workbench Moderation
; Base: 7.x-1.4
; Patch 35bfed20:  Upgrade from 1.3 to 1.4 with drush fails.  https://www.drupal.org/node/2428371
; e6e0ae3b: Revert "Issue #1492118 by joelpittet, jmuzz, nasia123, bbinkovitz, delphian: Set up access grants for the unpublished content access permission."  This broke publication ordering by adding a bad node access grant to each sql query.
projects[workbench_moderation][type] = "module"
projects[workbench_moderation][download][type] = "git"
projects[workbench_moderation][download][url] = "https://git.uwaterloo.ca/drupal-org/workbench_moderation.git"
projects[workbench_moderation][download][tag] = "7.x-1.4-uw_wcms2"
projects[workbench_moderation][subdir] = "contrib"

; WYSIWYG
; Base: 7.x-2.2
; Patch 1cb5c7e: Provide method to hide input format fields without disabling WYSIWYG. https://drupal.org/comment/6546146#comment-6546146 https://drupal.org/files/wysiwyg-one-format.934976.23-alt.patch
projects[wysiwyg][type] = "module"
projects[wysiwyg][download][type] = "git"
projects[wysiwyg][download][url] = "https://git.uwaterloo.ca/drupal-org/wysiwyg.git"
projects[wysiwyg][download][tag] = "7.x-2.2-uw_wcms2"
projects[wysiwyg][subdir] = "contrib"

; WYSIWYG Filter
projects[wysiwyg_filter][type] = "module"
projects[wysiwyg_filter][download][type] = "git"
projects[wysiwyg_filter][download][url] = "https://git.uwaterloo.ca/drupal-org/wysiwyg_filter.git"
projects[wysiwyg_filter][download][tag] = "7.x-1.6-rc2"
projects[wysiwyg_filter][subdir] = "contrib"

; XML sitemap
projects[xmlsitemap][type] = "module"
projects[xmlsitemap][download][type] = "git"
projects[xmlsitemap][download][url] = "https://git.uwaterloo.ca/drupal-org/xmlsitemap.git"
projects[xmlsitemap][download][tag] = "7.x-2.2"
projects[xmlsitemap][subdir] = "contrib"


;------------------------------------------------------------------------------------------------------------------------
; Custom Modules
; These modules are developed in-house
;------------------------------------------------------------------------------------------------------------------------

; CKEditor Social Media
projects[ckeditor_socialmedia][type] = "module"
projects[ckeditor_socialmedia][download][type] = "git"
projects[ckeditor_socialmedia][download][url] = "https://git.uwaterloo.ca/wcms/ckeditor_socialmedia.git"
projects[ckeditor_socialmedia][download][branch] = "master"
projects[ckeditor_socialmedia][subdir] = "custom"

; Date Friendly Abbr.
projects[date_friendly_abbr][type] = "module"
projects[date_friendly_abbr][download][type] = "git"
projects[date_friendly_abbr][download][url] = "https://git.uwaterloo.ca/wcms/date_friendly_abbr.git"
projects[date_friendly_abbr][download][branch] = "master"
projects[date_friendly_abbr][subdir] = "custom"

; Link autocomplete
projects[link_autocomplete][type] = "module"
projects[link_autocomplete][download][type] = "git"
projects[link_autocomplete][download][url] = "https://git.uwaterloo.ca/wcms/link_autocomplete.git"
projects[link_autocomplete][download][branch] = "master"
projects[link_autocomplete][subdir] = "custom"

; Media Link
projects[media_link][type] = "module"
projects[media_link][download][type] = "git"
projects[media_link][download][url] = "https://git.uwaterloo.ca/wcms/media_link.git"
projects[media_link][download][branch] = "master"
projects[media_link][subdir] = "custom"

; Menu Operations Permission module
projects[menu_operations_permission][type] = "module"
projects[menu_operations_permission][download][type] = "git"
projects[menu_operations_permission][download][url] = "https://git.uwaterloo.ca/wcms/menu_operations_permission.git"
projects[menu_operations_permission][download][branch] = "master"
projects[menu_operations_permission][subdir] = "custom"

; Menu Pathauto Fix
projects[menu_pathauto_fix][type] = "module"
projects[menu_pathauto_fix][download][type] = "git"
projects[menu_pathauto_fix][download][url] = "https://git.uwaterloo.ca/wcms/menu_pathauto_fix.git"
projects[menu_pathauto_fix][download][branch] = "master"
projects[menu_pathauto_fix][subdir] = "custom"

; uWaterloo Accessibility
projects[uw_a11y][type] = "module"
projects[uw_a11y][download][type] = "git"
projects[uw_a11y][download][url] = "https://git.uwaterloo.ca/wcms/uw_a11y.git"
projects[uw_a11y][download][branch] = "master"
projects[uw_a11y][subdir] = "custom"

; Admin usability - form menu
projects[uw_adminux_form_menu][type] = "module"
projects[uw_adminux_form_menu][download][type] = "git"
projects[uw_adminux_form_menu][download][url] = "https://git.uwaterloo.ca/wcms/uw_adminux_form_menu.git"
projects[uw_adminux_form_menu][download][branch] = "master"
projects[uw_adminux_form_menu][subdir] = "custom"

; uWaterloo CAPTCHA
projects[uw_captcha][type] = "module"
projects[uw_captcha][download][type] = "git"
projects[uw_captcha][download][url] = "https://git.uwaterloo.ca/wcms/uw_captcha.git"
projects[uw_captcha][download][branch] = "master"
projects[uw_captcha][subdir] = "custom"

; uWaterloo CAS username
projects[uw_cas_username][type] = "module"
projects[uw_cas_username][download][type] = "git"
projects[uw_cas_username][download][url] = "https://git.uwaterloo.ca/wcms/uw_cas_username.git"
projects[uw_cas_username][download][branch] = "master"
projects[uw_cas_username][subdir] = "custom"

; UW CKEditor Link Paths
projects[uw_ckeditor_link_paths][type] = "module"
projects[uw_ckeditor_link_paths][download][type] = "git"
projects[uw_ckeditor_link_paths][download][url] = "https://git.uwaterloo.ca/wcms/uw_ckeditor_link_paths.git"
projects[uw_ckeditor_link_paths][download][branch] = "master"
projects[uw_ckeditor_link_paths][subdir] = "custom"

; Clear Cache Block
projects[uw_clear_cache_block][type] = "module"
projects[uw_clear_cache_block][download][type] = "git"
projects[uw_clear_cache_block][download][url] = "https://git.uwaterloo.ca/wcms/uw_clear_cache_block.git"
projects[uw_clear_cache_block][download][branch] = "master"
projects[uw_clear_cache_block][subdir] = "custom"

; uWaterloo Decoupled Module [Publication]
projects[uw_decoupled_module][type] = "module"
projects[uw_decoupled_module][download][type] = "git"
projects[uw_decoupled_module][download][url] = "https://git.uwaterloo.ca/wcms-decoupled/uw_decoupled_module.git"
projects[uw_decoupled_module][download][branch] = "master"
projects[uw_decoupled_module][subdir] = "custom"

; Default Audience Categories
projects[uw_data_audience_categories][type] = "module"
projects[uw_data_audience_categories][download][type] = "git"
projects[uw_data_audience_categories][download][url] = "https://git.uwaterloo.ca/wcms/uw_data_audience_categories.git"
projects[uw_data_audience_categories][download][branch] = "master"
projects[uw_data_audience_categories][subdir] = "custom"

; Default Event types
projects[uw_data_event_type][type] = "module"
projects[uw_data_event_type][download][type] = "git"
projects[uw_data_event_type][download][url] = "https://git.uwaterloo.ca/wcms/uw_data_event_type.git"
projects[uw_data_event_type][download][branch] = "master"
projects[uw_data_event_type][subdir] = "custom"

; Default Feature Story Categories
projects[uw_data_feature_story_categories][type] = "module"
projects[uw_data_feature_story_categories][download][type] = "git"
projects[uw_data_feature_story_categories][download][url] = "https://git.uwaterloo.ca/wcms/uw_data_feature_story_categories.git"
projects[uw_data_feature_story_categories][download][branch] = "master"
projects[uw_data_feature_story_categories][subdir] = "custom"

; Default Profile Categories
projects[uw_data_profile_categories][type] = "module"
projects[uw_data_profile_categories][download][type] = "git"
projects[uw_data_profile_categories][download][url] = "https://git.uwaterloo.ca/wcms/uw_data_profile_categories.git"
projects[uw_data_profile_categories][download][branch] = "master"
projects[uw_data_profile_categories][subdir] = "custom"

; Default Web Pages and site IA, run during site install time and setup
projects[uw_data_web_pages][type] = "module"
projects[uw_data_web_pages][download][type] = "git"
projects[uw_data_web_pages][download][url] = "https://git.uwaterloo.ca/wcms/uw_data_web_pages.git"
projects[uw_data_web_pages][download][branch] = "master"
projects[uw_data_web_pages][subdir] = "custom"

; Display Writer Tips
projects[uw_display_writer_tips][type] = "module"
projects[uw_display_writer_tips][download][type] = "git"
projects[uw_display_writer_tips][download][url] = "https://git.uwaterloo.ca/wcms/uw_display_writer_tips.git"
projects[uw_display_writer_tips][download][branch] = "master"
projects[uw_display_writer_tips][subdir] = "custom"

; uWaterloo Tableau Field
projects[uw_field_tableau][type] = "module"
projects[uw_field_tableau][download][type] = "git"
projects[uw_field_tableau][download][url] = "https://git.uwaterloo.ca/wcms/uw_field_tableau.git"
projects[uw_field_tableau][download][branch] = "master"
projects[uw_field_tableau][subdir] = "custom"

; uWaterloo Filters
projects[uw_filters][type] = "module"
projects[uw_filters][download][type] = "git"
projects[uw_filters][download][url] = "https://git.uwaterloo.ca/wcms/uw_filters.git"
projects[uw_filters][download][branch] = "master"
projects[uw_filters][subdir] = "custom"

; Web Form Additional Functionality
projects[uw_forms][type] = "module"
projects[uw_forms][download][type] = "git"
projects[uw_forms][download][url] = "https://git.uwaterloo.ca/wcms/uw_forms.git"
projects[uw_forms][download][branch] = "master"
projects[uw_forms][subdir] = "custom"

; uWaterloo LDAP
projects[uw_ldap][type] = "module"
projects[uw_ldap][download][type] = "git"
projects[uw_ldap][download][url] = "https://git.uwaterloo.ca/wcms/uw_ldap.git"
projects[uw_ldap][download][branch] = "master"
projects[uw_ldap][subdir] = "custom"

; uWaterloo Node Reference Insert [Publication]
projects[uw_node_reference_insert][type] = "module"
projects[uw_node_reference_insert][download][type] = "git"
projects[uw_node_reference_insert][download][url] = "https://git.uwaterloo.ca/wcms/uw_node_reference_insert.git"
projects[uw_node_reference_insert][download][tag] = "7.x-1.0"
projects[uw_node_reference_insert][subdir] = "custom"

; uWaterloo Org Chart
projects[uw_org_chart][type] = "module"
projects[uw_org_chart][download][type] = "git"
projects[uw_org_chart][download][url] = "https://git.uwaterloo.ca/wcms/uw_org_chart.git"
projects[uw_org_chart][download][branch] = "master"
projects[uw_org_chart][subdir] = "custom"

; uWaterloo Page settings
projects[uw_page_settings_node][type] = "module"
projects[uw_page_settings_node][download][type] = "git"
projects[uw_page_settings_node][download][url] = "https://git.uwaterloo.ca/wcms/uw_page_settings_node.git"
projects[uw_page_settings_node][download][branch] = "master"
projects[uw_page_settings_node][subdir] = "custom"

; uWaterloo RESTful Publication [Publication]
projects[uw_restful_publication][type] = "module"
projects[uw_restful_publication][download][type] = "git"
projects[uw_restful_publication][download][url] = "https://git.uwaterloo.ca/wcms-decoupled/uw_restful_publication.git"
projects[uw_restful_publication][download][branch] = "master"
projects[uw_restful_publication][subdir] = "custom"

; uWaterloo security
projects[uw_security][type] = "module"
projects[uw_security][download][type] = "git"
projects[uw_security][download][url] = "https://git.uwaterloo.ca/wcms/uw_security.git"
projects[uw_security][download][branch] = "master"
projects[uw_security][subdir] = "custom"

; uWaterloo Student Budget Calculator
projects[uw_studentbudget_calculator][type] = "module"
projects[uw_studentbudget_calculator][download][type] = "git"
projects[uw_studentbudget_calculator][download][url] = "https://git.uwaterloo.ca/wcms/uw_studentbudget_calculator.git"
projects[uw_studentbudget_calculator][download][branch] = "master"
projects[uw_studentbudget_calculator][subdir] = "custom"

; uWaterloo Taxonomy Rights Access
projects[uw_taxonomy_rights_access][type] = "module"
projects[uw_taxonomy_rights_access][download][type] = "git"
projects[uw_taxonomy_rights_access][download][url] = "https://git.uwaterloo.ca/wcms/uw_taxonomy_rights_access.git"
projects[uw_taxonomy_rights_access][download][branch] = "master"
projects[uw_taxonomy_rights_access][subdir] = "custom"

; uWaterloo Video Embed
projects[uw_video_embed][type] = "module"
projects[uw_video_embed][download][type] = "git"
projects[uw_video_embed][download][url] = "https://git.uwaterloo.ca/wcms/uw_video_embed.git"
projects[uw_video_embed][download][branch] = "master"
projects[uw_video_embed][subdir] = "custom"

; uWaterloo OFIS
projects[uw_ws_ofis][type] = "module"
projects[uw_ws_ofis][download][type] = "git"
projects[uw_ws_ofis][download][url] = "https://git.uwaterloo.ca/wcms/uw_ws_ofis.git"
projects[uw_ws_ofis][download][branch] = "master"
projects[uw_ws_ofis][subdir] = "custom"

; Views Rename
projects[views_rename][type] = "module"
projects[views_rename][download][type] = "git"
projects[views_rename][download][url] = "https://git.uwaterloo.ca/wcms/views_rename.git"
projects[views_rename][download][branch] = "master"
projects[views_rename][subdir] = "custom"


;------------------------------------------------------------------------------------------------------------------------
; Development Modules
; These modules are used primarily by developers
;------------------------------------------------------------------------------------------------------------------------

; Coder
projects[coder][type] = "module"
projects[coder][download][type] = "git"
projects[coder][download][url] = "https://git.uwaterloo.ca/drupal-org/coder.git"
projects[coder][download][tag] = "7.x-2.5"
projects[coder][subdir] = "dev"

; Devel
projects[devel][type] = "module"
projects[devel][download][type] = "git"
projects[devel][download][url] = "https://git.uwaterloo.ca/drupal-org/devel.git"
projects[devel][download][tag] = "7.x-1.5"
projects[devel][subdir] = "dev"

; Theme developer
projects[devel_themer][type] = "module"
projects[devel_themer][download][type] = "git"
projects[devel_themer][download][url] = "https://git.uwaterloo.ca/drupal-org/devel_themer.git"
projects[devel_themer][download][branch] = "7.x-1.x"
projects[devel_themer][download][revision] = "bec0859"
projects[devel_themer][subdir] = "dev"

; Drupal Reset
projects[drupal_reset][type] = "module"
projects[drupal_reset][download][type] = "git"
projects[drupal_reset][download][url] = "https://git.uwaterloo.ca/drupal-org/drupal_reset.git"
projects[drupal_reset][download][tag] = "7.x-1.4"
projects[drupal_reset][subdir] = "dev"

; Module filter
; base: 7.x-2.0
; Patch 91779bf4: #1969856 Incompatibility with Tab mode and Coder Review module. https://www.drupal.org/node/1969856
projects[module_filter][type] = "module"
projects[module_filter][download][type] = "git"
projects[module_filter][download][url] = "https://git.uwaterloo.ca/drupal-org/module_filter.git"
projects[module_filter][download][tag] = "7.x-2.0-uw_wcms1"
projects[module_filter][subdir] = "dev"

; simplehtmldom API
projects[simplehtmldom][type] = "module"
projects[simplehtmldom][download][type] = "git"
projects[simplehtmldom][download][url] = "https://git.uwaterloo.ca/drupal-org/simplehtmldom.git"
projects[simplehtmldom][download][tag] = "7.x-1.12"
projects[simplehtmldom][subdir] = "dev"

; Search Krumo
projects[search_krumo][type] = "module"
projects[search_krumo][download][type] = "git"
projects[search_krumo][download][url] = "https://git.uwaterloo.ca/drupal-org/search_krumo.git"
projects[search_krumo][download][tag] = "7.x-1.6"
projects[search_krumo][subdir] = "dev"

; Taxonomy CSV import/export
projects[taxonomy_csv][type] = "module"
projects[taxonomy_csv][download][type] = "git"
projects[taxonomy_csv][download][url] = "https://git.uwaterloo.ca/drupal-org/taxonomy_csv.git"
projects[taxonomy_csv][download][branch] = "7.x-5.x"
projects[taxonomy_csv][download][revision] = "539e061"
projects[taxonomy_csv][subdir] = "dev"

; Assign WCMS Groups from Active Directory
projects[uw_auth_wcms_admins][type] = "module"
projects[uw_auth_wcms_admins][download][type] = "git"
projects[uw_auth_wcms_admins][download][url] = "https://git.uwaterloo.ca/wcms/uw_auth_wcms_admins.git"
projects[uw_auth_wcms_admins][download][branch] = "master"
projects[uw_auth_wcms_admins][subdir] = "dev"

; Variable
projects[variable][type] = "module"
projects[variable][download][type] = "git"
projects[variable][download][url] = "https://git.uwaterloo.ca/drupal-org/variable.git"
projects[variable][download][tag] = "7.x-2.5"
projects[variable][subdir] = "dev"


;------------------------------------------------------------------------------------------------------------------------
; Features
; Modules that store config information in code instead of the database
;------------------------------------------------------------------------------------------------------------------------

; uw_anonymous_blog_comment_overrides
projects[uw_anonymous_blog_comment_overrides][type] = "module"
projects[uw_anonymous_blog_comment_overrides][download][type] = "git"
projects[uw_anonymous_blog_comment_overrides][download][url] = "https://git.uwaterloo.ca/wcms/uw_anonymous_blog_comment_overrides.git"
projects[uw_anonymous_blog_comment_overrides][download][branch] = "master"
projects[uw_anonymous_blog_comment_overrides][subdir] = "features"

; CAS Authentication Common
projects[uw_auth_cas_common][type] = "module"
projects[uw_auth_cas_common][download][type] = "git"
projects[uw_auth_cas_common][download][url] = "https://git.uwaterloo.ca/wcms/uw_auth_cas_common.git"
projects[uw_auth_cas_common][download][branch] = "master"
projects[uw_auth_cas_common][subdir] = "features"

; Authentication required site
projects[uw_auth_site][type] = "module"
projects[uw_auth_site][download][type] = "git"
projects[uw_auth_site][download][url] = "https://git.uwaterloo.ca/wcms/uw_auth_site.git"
projects[uw_auth_site][download][branch] = "master"
projects[uw_auth_site][subdir] = "features"

; Chinese Views Config
projects[uw_cfg_chinese_views][type] = "module"
projects[uw_cfg_chinese_views][download][type] = "git"
projects[uw_cfg_chinese_views][download][url] = "https://git.uwaterloo.ca/wcms/uw_cfg_chinese_views.git"
projects[uw_cfg_chinese_views][download][branch] = "master"
projects[uw_cfg_chinese_views][subdir] = "features"

; Clone Web Pages Config
projects[uw_cfg_clone_web_pages][type] = "module"
projects[uw_cfg_clone_web_pages][download][type] = "git"
projects[uw_cfg_clone_web_pages][download][url] = "https://git.uwaterloo.ca/wcms/uw_cfg_clone_web_pages.git"
projects[uw_cfg_clone_web_pages][download][branch] = "master"
projects[uw_cfg_clone_web_pages][subdir] = "features"

; Content Lock Config
projects[uw_cfg_content_lock][type] = "module"
projects[uw_cfg_content_lock][download][type] = "git"
projects[uw_cfg_content_lock][download][url] = "https://git.uwaterloo.ca/wcms/uw_cfg_content_lock.git"
projects[uw_cfg_content_lock][download][branch] = "master"
projects[uw_cfg_content_lock][subdir] = "features"

; Devel Config
projects[uw_cfg_devel][type] = "module"
projects[uw_cfg_devel][download][type] = "git"
projects[uw_cfg_devel][download][url] = "https://git.uwaterloo.ca/wcms/uw_cfg_devel.git"
projects[uw_cfg_devel][download][branch] = "master"
projects[uw_cfg_devel][subdir] = "features"

; uWaterloo Expire Config
projects[uw_cfg_expire][type] = "module"
projects[uw_cfg_expire][download][type] = "git"
projects[uw_cfg_expire][download][url] = "https://git.uwaterloo.ca/wcms/uw_cfg_expire.git"
projects[uw_cfg_expire][download][branch] = "master"
projects[uw_cfg_expire][subdir] = "features"

; Google Analytics Config
projects[uw_cfg_google_analytics][type] = "module"
projects[uw_cfg_google_analytics][download][type] = "git"
projects[uw_cfg_google_analytics][download][url] = "https://git.uwaterloo.ca/wcms/uw_cfg_google_analytics.git"
projects[uw_cfg_google_analytics][download][branch] = "master"
projects[uw_cfg_google_analytics][subdir] = "features"

; Link Checker Config
projects[uw_cfg_linkchecker][type] = "module"
projects[uw_cfg_linkchecker][download][type] = "git"
projects[uw_cfg_linkchecker][download][url] = "https://git.uwaterloo.ca/wcms/uw_cfg_linkchecker.git"
projects[uw_cfg_linkchecker][download][branch] = "master"
projects[uw_cfg_linkchecker][subdir] = "features"

; uWaterloo MathJax Config
projects[uw_cfg_mathjax][type] = "module"
projects[uw_cfg_mathjax][download][type] = "git"
projects[uw_cfg_mathjax][download][url] = "https://git.uwaterloo.ca/wcms/uw_cfg_mathjax.git"
projects[uw_cfg_mathjax][download][branch] = "master"
projects[uw_cfg_mathjax][subdir] = "features"

; uWaterloo Multilingual Config
projects[uw_cfg_multilingual][type] = "module"
projects[uw_cfg_multilingual][download][type] = "git"
projects[uw_cfg_multilingual][download][url] = "https://git.uwaterloo.ca/wcms/uw_cfg_multilingual.git"
projects[uw_cfg_multilingual][download][branch] = "master"
projects[uw_cfg_multilingual][subdir] = "features"

; Social Media Sharing Config
projects[uw_cfg_media_sharing][type] = "module"
projects[uw_cfg_media_sharing][download][type] = "git"
projects[uw_cfg_media_sharing][download][url] = "https://git.uwaterloo.ca/wcms/uw_cfg_media_sharing.git"
projects[uw_cfg_media_sharing][download][branch] = "master"
projects[uw_cfg_media_sharing][subdir] = "features"

; Menu Pathauto Fix Config
projects[uw_cfg_menu_pathauto_fix][type] = "module"
projects[uw_cfg_menu_pathauto_fix][download][type] = "git"
projects[uw_cfg_menu_pathauto_fix][download][url] = "https://git.uwaterloo.ca/wcms/uw_cfg_menu_pathauto_fix.git"
projects[uw_cfg_menu_pathauto_fix][download][branch] = "master"
projects[uw_cfg_menu_pathauto_fix][subdir] = "features"

; Metatag Config
projects[uw_cfg_metatag][type] = "module"
projects[uw_cfg_metatag][download][type] = "git"
projects[uw_cfg_metatag][download][url] = "https://git.uwaterloo.ca/wcms/uw_cfg_metatag.git"
projects[uw_cfg_metatag][download][branch] = "master"
projects[uw_cfg_metatag][subdir] = "features"

; More Buttons Config
projects[uw_cfg_more_buttons][type] = "module"
projects[uw_cfg_more_buttons][download][type] = "git"
projects[uw_cfg_more_buttons][download][url] = "https://git.uwaterloo.ca/wcms/uw_cfg_more_buttons.git"
projects[uw_cfg_more_buttons][download][branch] = "master"
projects[uw_cfg_more_buttons][subdir] = "features"

; Page Title Config
projects[uw_cfg_page_title][type] = "module"
projects[uw_cfg_page_title][download][type] = "git"
projects[uw_cfg_page_title][download][url] = "https://git.uwaterloo.ca/wcms/uw_cfg_page_title.git"
projects[uw_cfg_page_title][download][branch] = "master"
projects[uw_cfg_page_title][subdir] = "features"

; Redirect Config
projects[uw_cfg_redirect][type] = "module"
projects[uw_cfg_redirect][download][type] = "git"
projects[uw_cfg_redirect][download][url] = "https://git.uwaterloo.ca/wcms/uw_cfg_redirect.git"
projects[uw_cfg_redirect][download][branch] = "master"
projects[uw_cfg_redirect][subdir] = "features"

; User Protect Configuration
projects[uw_cfg_user_protect][type] = "module"
projects[uw_cfg_user_protect][download][type] = "git"
projects[uw_cfg_user_protect][download][url] = "https://git.uwaterloo.ca/wcms/uw_cfg_user_protect.git"
projects[uw_cfg_user_protect][download][branch] = "master"
projects[uw_cfg_user_protect][subdir] = "features"

; OFIS Config
projects[uw_cfg_ws_ofis][type] = "module"
projects[uw_cfg_ws_ofis][download][type] = "git"
projects[uw_cfg_ws_ofis][download][url] = "https://git.uwaterloo.ca/wcms/uw_cfg_ws_ofis.git"
projects[uw_cfg_ws_ofis][download][branch] = "master"
projects[uw_cfg_ws_ofis][subdir] = "features"

; Award
projects[uw_ct_award][type] = "module"
projects[uw_ct_award][download][type] = "git"
projects[uw_ct_award][download][url] = "https://git.uwaterloo.ca/wcms/uw_ct_award.git"
projects[uw_ct_award][download][branch] = "master"
projects[uw_ct_award][subdir] = "features"

; Bibliography
projects[uw_ct_bibliography][type] = "module"
projects[uw_ct_bibliography][download][type] = "git"
projects[uw_ct_bibliography][download][url] = "https://git.uwaterloo.ca/wcms/uw_ct_bibliography.git"
projects[uw_ct_bibliography][download][branch] = "master"
projects[uw_ct_bibliography][subdir] = "features"

; Blog
projects[uw_ct_blog][type] = "module"
projects[uw_ct_blog][download][type] = "git"
projects[uw_ct_blog][download][url] = "https://git.uwaterloo.ca/wcms/uw_ct_blog.git"
projects[uw_ct_blog][download][branch] = "master"
projects[uw_ct_blog][subdir] = "features"

; Contact
projects[uw_ct_contact][type] = "module"
projects[uw_ct_contact][download][type] = "git"
projects[uw_ct_contact][download][url] = "https://git.uwaterloo.ca/wcms/uw_ct_contact.git"
projects[uw_ct_contact][download][branch] = "master"
projects[uw_ct_contact][subdir] = "features"

; Custom Listing Page
projects[uw_ct_custom_listing][type] = "module"
projects[uw_ct_custom_listing][download][type] = "git"
projects[uw_ct_custom_listing][download][url] = "https://git.uwaterloo.ca/wcms/uw_ct_custom_listing.git"
projects[uw_ct_custom_listing][download][branch] = "master"
projects[uw_ct_custom_listing][subdir] = "features"

; Embedded Facts and Figures
projects[uw_ct_embedded_facts_and_figures][type] = "module"
projects[uw_ct_embedded_facts_and_figures][download][type] = "git"
projects[uw_ct_embedded_facts_and_figures][download][url] = "https://git.uwaterloo.ca/wcms/uw_ct_embedded_facts_and_figures.git"
projects[uw_ct_embedded_facts_and_figures][download][branch] = "master"
projects[uw_ct_embedded_facts_and_figures][subdir] = "features"

; Embedded Timeline
projects[uw_ct_embedded_timeline][type] = "module"
projects[uw_ct_embedded_timeline][download][type] = "git"
projects[uw_ct_embedded_timeline][download][url] = "https://git.uwaterloo.ca/wcms/uw_ct_embedded_timeline.git"
projects[uw_ct_embedded_timeline][download][branch] = "master"
projects[uw_ct_embedded_timeline][subdir] = "features"

; Event
projects[uw_ct_event][type] = "module"
projects[uw_ct_event][download][type] = "git"
projects[uw_ct_event][download][url] = "https://git.uwaterloo.ca/wcms/uw_ct_event.git"
projects[uw_ct_event][download][branch] = "master"
projects[uw_ct_event][subdir] = "features"

; Home Page Banner
projects[uw_ct_home_page_banner][type] = "module"
projects[uw_ct_home_page_banner][download][type] = "git"
projects[uw_ct_home_page_banner][download][url] = "https://git.uwaterloo.ca/wcms/uw_ct_home_page_banner.git"
projects[uw_ct_home_page_banner][download][branch] = "master"
projects[uw_ct_home_page_banner][subdir] = "features"

; Image Gallery
projects[uw_ct_image_gallery][type] = "module"
projects[uw_ct_image_gallery][download][type] = "git"
projects[uw_ct_image_gallery][download][url] = "https://git.uwaterloo.ca/wcms/uw_ct_image_gallery.git"
projects[uw_ct_image_gallery][download][branch] = "master"
projects[uw_ct_image_gallery][subdir] = "features"

; News Item
projects[uw_ct_news_item][type] = "module"
projects[uw_ct_news_item][download][type] = "git"
projects[uw_ct_news_item][download][url] = "https://git.uwaterloo.ca/wcms/uw_ct_news_item.git"
projects[uw_ct_news_item][download][branch] = "master"
projects[uw_ct_news_item][subdir] = "features"

; Opportunities
projects[uw_ct_opportunities][type] = "module"
projects[uw_ct_opportunities][download][type] = "git"
projects[uw_ct_opportunities][download][url] = "https://git.uwaterloo.ca/wcms/uw_ct_opportunities.git"
projects[uw_ct_opportunities][download][branch] = "master"
projects[uw_ct_opportunities][subdir] = "features"

; Person Profile
projects[uw_ct_person_profile][type] = "module"
projects[uw_ct_person_profile][download][type] = "git"
projects[uw_ct_person_profile][download][url] = "https://git.uwaterloo.ca/wcms/uw_ct_person_profile.git"
projects[uw_ct_person_profile][download][branch] = "master"
projects[uw_ct_person_profile][subdir] = "features"

; Project
projects[uw_ct_project][type] = "module"
projects[uw_ct_project][download][type] = "git"
projects[uw_ct_project][download][url] = "https://git.uwaterloo.ca/wcms/uw_ct_project.git"
projects[uw_ct_project][download][branch] = "master"
projects[uw_ct_project][subdir] = "features"

; Promotional Item
projects[uw_ct_promo_item][type] = "module"
projects[uw_ct_promo_item][download][type] = "git"
projects[uw_ct_promo_item][download][url] = "https://git.uwaterloo.ca/wcms/uw_ct_promo_item.git"
projects[uw_ct_promo_item][download][branch] = "master"
projects[uw_ct_promo_item][subdir] = "features"

; uWaterloo Publication [Publication]
projects[uw_cfg_publication][type] = "module"
projects[uw_cfg_publication][download][type] = "git"
projects[uw_cfg_publication][download][url] = "https://git.uwaterloo.ca/wcms/uw_cfg_publication.git"
projects[uw_cfg_publication][download][branch] = "master"
projects[uw_cfg_publication][subdir] = "features"

; Service
projects[uw_ct_service][type] = "module"
projects[uw_ct_service][download][type] = "git"
projects[uw_ct_service][download][url] = "https://git.uwaterloo.ca/wcms/uw_ct_service.git"
projects[uw_ct_service][download][branch] = "master"
projects[uw_ct_service][subdir] = "features"

; Special Alert
projects[uw_ct_special_alert][type] = "module"
projects[uw_ct_special_alert][download][type] = "git"
projects[uw_ct_special_alert][download][url] = "https://git.uwaterloo.ca/wcms/uw_ct_special_alert.git"
projects[uw_ct_special_alert][download][branch] = "master"
projects[uw_ct_special_alert][subdir] = "features"

; Web Form
projects[uw_ct_web_form][type] = "module"
projects[uw_ct_web_form][download][type] = "git"
projects[uw_ct_web_form][download][url] = "https://git.uwaterloo.ca/wcms/uw_ct_web_form.git"
projects[uw_ct_web_form][download][branch] = "master"
projects[uw_ct_web_form][subdir] = "features"

; Web Page
projects[uw_ct_web_page][type] = "module"
projects[uw_ct_web_page][download][type] = "git"
projects[uw_ct_web_page][download][url] = "https://git.uwaterloo.ca/wcms/uw_ct_web_page.git"
projects[uw_ct_web_page][download][branch] = "master"
projects[uw_ct_web_page][subdir] = "features"

; FDSU Dashboard
projects[uw_dashboard_fdsu][type] = "module"
projects[uw_dashboard_fdsu][download][type] = "git"
projects[uw_dashboard_fdsu][download][url] = "https://git.uwaterloo.ca/wcms/uw_dashboard_fdsu.git"
projects[uw_dashboard_fdsu][download][branch] = "master"
projects[uw_dashboard_fdsu][subdir] = "features"

; Daily Bulletin Feed for uWaterloo Main
projects[uw_feed_bulletin_main][type] = "module"
projects[uw_feed_bulletin_main][download][type] = "git"
projects[uw_feed_bulletin_main][download][url] = "https://git.uwaterloo.ca/wcms/uw_feed_bulletin_main.git"
projects[uw_feed_bulletin_main][download][branch] = "master"
projects[uw_feed_bulletin_main][subdir] = "features"

; Main Homepage Events Feed
projects[uw_feed_events_main][type] = "module"
projects[uw_feed_events_main][download][type] = "git"
projects[uw_feed_events_main][download][url] = "https://git.uwaterloo.ca/wcms/uw_feed_events_main.git"
projects[uw_feed_events_main][download][branch] = "master"
projects[uw_feed_events_main][subdir] = "features"

; Web Resources News Feed
projects[uw_feed_webresources_news][type] = "module"
projects[uw_feed_webresources_news][download][type] = "git"
projects[uw_feed_webresources_news][download][url] = "https://git.uwaterloo.ca/wcms/uw_feed_webresources_news.git"
projects[uw_feed_webresources_news][download][branch] = "master"
projects[uw_feed_webresources_news][subdir] = "features"

; uWaterloo Gmap Config
projects[uw_gmap_config][type] = "module"
projects[uw_gmap_config][download][type] = "git"
projects[uw_gmap_config][download][url] = "https://git.uwaterloo.ca/wcms/uw_gmap_config.git"
projects[uw_gmap_config][download][branch] = "master"
projects[uw_gmap_config][subdir] = "features"

; uWaterloo IMCE Config
projects[uw_imce_config][type] = "module"
projects[uw_imce_config][download][type] = "git"
projects[uw_imce_config][download][url] = "https://git.uwaterloo.ca/wcms/uw_imce_config.git"
projects[uw_imce_config][download][branch] = "master"
projects[uw_imce_config][subdir] = "features"

; uWaterloo Less Transliteration
projects[uw_less_transliteration][type] = "module"
projects[uw_less_transliteration][download][type] = "git"
projects[uw_less_transliteration][download][url] = "https://git.uwaterloo.ca/wcms/uw_less_transliteration.git"
projects[uw_less_transliteration][download][branch] = "master"
projects[uw_less_transliteration][subdir] = "features"

; Audience Menu
projects[uw_nav_audience_menu][type] = "module"
projects[uw_nav_audience_menu][download][type] = "git"
projects[uw_nav_audience_menu][download][url] = "https://git.uwaterloo.ca/wcms/uw_nav_audience_menu.git"
projects[uw_nav_audience_menu][download][branch] = "master"
projects[uw_nav_audience_menu][subdir] = "features"

; Global Footer
projects[uw_nav_global_footer][type] = "module"
projects[uw_nav_global_footer][download][type] = "git"
projects[uw_nav_global_footer][download][url] = "https://git.uwaterloo.ca/wcms/uw_nav_global_footer.git"
projects[uw_nav_global_footer][download][branch] = "master"
projects[uw_nav_global_footer][subdir] = "features"

; Main Menu
projects[uw_nav_main_menu][type] = "module"
projects[uw_nav_main_menu][download][type] = "git"
projects[uw_nav_main_menu][download][url] = "https://git.uwaterloo.ca/wcms/uw_nav_main_menu.git"
projects[uw_nav_main_menu][download][branch] = "master"
projects[uw_nav_main_menu][subdir] = "features"

; Menu Operations Permission Config
projects[uw_nav_menu_operations_permission][type] = "module"
projects[uw_nav_menu_operations_permission][download][type] = "git"
projects[uw_nav_menu_operations_permission][download][url] = "https://git.uwaterloo.ca/wcms/uw_nav_menu_operations_permission.git"
projects[uw_nav_menu_operations_permission][download][branch] = "master"
projects[uw_nav_menu_operations_permission][subdir] = "features"

; Site Footer
projects[uw_nav_site_footer][type] = "module"
projects[uw_nav_site_footer][download][type] = "git"
projects[uw_nav_site_footer][download][url] = "https://git.uwaterloo.ca/wcms/uw_nav_site_footer.git"
projects[uw_nav_site_footer][download][branch] = "master"
projects[uw_nav_site_footer][subdir] = "features"

; FDSU Roles
projects[uw_roles_fdsu][type] = "module"
projects[uw_roles_fdsu][download][type] = "git"
projects[uw_roles_fdsu][download][url] = "https://git.uwaterloo.ca/wcms/uw_roles_fdsu.git"
projects[uw_roles_fdsu][download][branch] = "master"
projects[uw_roles_fdsu][subdir] = "features"

; FDSU Site Controller
projects[uw_site_fdsu][type] = "module"
projects[uw_site_fdsu][download][type] = "git"
projects[uw_site_fdsu][download][url] = "https://git.uwaterloo.ca/wcms/uw_site_fdsu.git"
projects[uw_site_fdsu][download][branch] = "master"
projects[uw_site_fdsu][subdir] = "features"

; User Management
projects[uw_user_management][type] = "module"
projects[uw_user_management][download][type] = "git"
projects[uw_user_management][download][url] = "https://git.uwaterloo.ca/wcms/uw_user_management.git"
projects[uw_user_management][download][branch] = "master"
projects[uw_user_management][subdir] = "features"

; uWaterloo Value Lists
projects[uw_value_lists][type] = "module"
projects[uw_value_lists][download][type] = "git"
projects[uw_value_lists][download][url] = "https://git.uwaterloo.ca/wcms/uw_value_lists.git"
projects[uw_value_lists][download][branch] = "master"
projects[uw_value_lists][subdir] = "features"

; Audience Vocabulary
projects[uw_vocab_audience][type] = "module"
projects[uw_vocab_audience][download][type] = "git"
projects[uw_vocab_audience][download][url] = "https://git.uwaterloo.ca/wcms/uw_vocab_audience.git"
projects[uw_vocab_audience][download][branch] = "master"
projects[uw_vocab_audience][subdir] = "features"

; Profiles Vocabulary
projects[uw_vocab_profiles][type] = "module"
projects[uw_vocab_profiles][download][type] = "git"
projects[uw_vocab_profiles][download][url] = "https://git.uwaterloo.ca/wcms/uw_vocab_profiles.git"
projects[uw_vocab_profiles][download][branch] = "master"
projects[uw_vocab_profiles][subdir] = "features"

; Term Lock
projects[uw_vocab_term_lock][type] = "module"
projects[uw_vocab_term_lock][download][type] = "git"
projects[uw_vocab_term_lock][download][url] = "https://git.uwaterloo.ca/wcms/uw_vocab_term_lock.git"
projects[uw_vocab_term_lock][download][branch] = "master"
projects[uw_vocab_term_lock][subdir] = "features"

; Workbench Config
projects[uw_wb_config][type] = "module"
projects[uw_wb_config][download][type] = "git"
projects[uw_wb_config][download][url] = "https://git.uwaterloo.ca/wcms/uw_wb_config.git"
projects[uw_wb_config][download][branch] = "master"
projects[uw_wb_config][subdir] = "features"

; CKEditor Config
projects[uw_wysiwyg_ckeditor][type] = "module"
projects[uw_wysiwyg_ckeditor][download][type] = "git"
projects[uw_wysiwyg_ckeditor][download][url] = "https://git.uwaterloo.ca/wcms/uw_wysiwyg_ckeditor.git"
projects[uw_wysiwyg_ckeditor][download][branch] = "master"
projects[uw_wysiwyg_ckeditor][subdir] = "features"

; UWaterloo Theme Vocabulary [Publication]
projects[uw_vocab_uwaterloo_theme][type] = "module"
projects[[uw_vocab_uwaterloo_theme[download][type] = "git"
projects[uw_vocab_uwaterloo_theme][download][url] = "https://git.uwaterloo.ca/wcms/uw_vocab_uwaterloo_theme.git"
projects[uw_vocab_uwaterloo_theme][download][branch] = "master"
projects[uw_vocab_uwaterloo_theme][subdir] = "features"

; XML Sitemap Config
projects[uw_xml_sitemap_config][type] = "module"
projects[uw_xml_sitemap_config][download][type] = "git"
projects[uw_xml_sitemap_config][download][url] = "https://git.uwaterloo.ca/wcms/uw_xml_sitemap_config.git"
projects[uw_xml_sitemap_config][download][branch] = "master"
projects[uw_xml_sitemap_config][subdir] = "features"


;------------------------------------------------------------------------------------------------------------------------
; Themes
;------------------------------------------------------------------------------------------------------------------------

; Adminimal Theme
projects[adminimal_theme][type] = "theme"
projects[adminimal_theme][download][type] = "git"
projects[adminimal_theme][download][url] = "https://git.uwaterloo.ca/drupal-org/adminimal_theme.git"
projects[adminimal_theme][download][tag] = "7.x-1.22"

; uWaterloo Admin Theme
projects[uw_admin_theme][type] = "theme"
projects[uw_admin_theme][download][type] = "git"
projects[uw_admin_theme][download][url] = "https://git.uwaterloo.ca/wcms/uw_admin_theme.git"
projects[uw_admin_theme][download][branch] = "master"

; uWaterloo Adminimal Theme
projects[uw_adminimal_theme][type] = "theme"
projects[uw_adminimal_theme][download][type] = "git"
projects[uw_adminimal_theme][download][url] = "https://git.uwaterloo.ca/wcms/uw_adminimal_theme.git"
projects[uw_adminimal_theme][download][branch] = "master"

; uWaterloo Core Theme (base)
projects[uw_core_theme][type] = "theme"
projects[uw_core_theme][download][type] = "git"
projects[uw_core_theme][download][url] = "https://git.uwaterloo.ca/wcms/uw_core_theme.git"
projects[uw_core_theme][download][branch] = "master"

; uWaterloo Decoupled Front-end Theme [Publication]
projects[uw_custom_frontend][type] = "theme"
projects[uw_custom_frontend][download][type] = "git"
projects[uw_custom_frontend][download][url] = "https://git.uwaterloo.ca/wcms-decoupled/uw_custom_frontend.git"
projects[uw_custom_frontend][download][branch] = "uw_publication"

; uWaterloo FDSU Theme
projects[uw_fdsu_theme][type] = "theme"
projects[uw_fdsu_theme][download][type] = "git"
projects[uw_fdsu_theme][download][url] = "https://git.uwaterloo.ca/wcms/uw_fdsu_theme.git"
projects[uw_fdsu_theme][download][branch] = "master"

; uWaterloo FDSU Theme Responsive
projects[uw_fdsu_theme_resp][type] = "theme"
projects[uw_fdsu_theme_resp][download][type] = "git"
projects[uw_fdsu_theme_resp][download][url] = "https://git.uwaterloo.ca/wcms/uw_fdsu_theme_resp.git"
projects[uw_fdsu_theme_resp][download][branch] = "master"

; uWaterloo Generic Theme
projects[uw_generic_theme][type] = "theme"
projects[uw_generic_theme][download][type] = "git"
projects[uw_generic_theme][download][url] = "https://git.uwaterloo.ca/wcms/uw_generic_theme.git"
projects[uw_generic_theme][download][branch] = "master"

; uWaterloo Generic Theme for French Site
projects[uw_generic_theme_fr][type] = "theme"
projects[uw_generic_theme_fr][download][type] = "git"
projects[uw_generic_theme_fr][download][url] = "https://git.uwaterloo.ca/wcms/uw_generic_theme_fr.git"
projects[uw_generic_theme_fr][download][branch] = "master"

; uWaterloo Publication Theme [Publication]
projects[uw_theme_publication][type] = "theme"
projects[uw_theme_publication][download][type] = "git"
projects[uw_theme_publication][download][url] = "https://git.uwaterloo.ca/wcms-decoupled/uw_theme_publication.git"
projects[uw_theme_publication][download][branch] = "master"


;------------------------------------------------------------------------------------------------------------------------
; Libraries
;------------------------------------------------------------------------------------------------------------------------

; Backbone.js
; http://backbonejs.org
libraries[backbonejs][download][type] = "git"
libraries[backbonejs][download][url] = "https://git.uwaterloo.ca/libraries/backbonejs.git"
libraries[backbonejs][download][tag] = "1.2.1"
libraries[backbonejs][directory_name] = "backbone"

; d3
; http://d3js.org/
libraries[d3][download][type] = "git"
libraries[d3][download][url] = "https://git.uwaterloo.ca/libraries/d3.git"
libraries[d3][download][branch] = "master"
libraries[d3][download][revision] = "5b981a18"

; phpCAS
; https://github.com/Jasig/phpCAS
libraries[CAS][download][type] = "git"
libraries[CAS][download][url] = "https://git.uwaterloo.ca/libraries/phpcas.git"
libraries[CAS][download][tag] = "1.3.3"
libraries[CAS][directory_name] = "CAS"

; CKEditor 4
; http://ckeditor.com/download
libraries[ckeditor][download][type] = "git"
libraries[ckeditor][download][url] = "https://git.uwaterloo.ca/libraries/ckeditor.git"
libraries[ckeditor][download][tag] = "4.4.7-full"

; ColorPicker
; http://www.eyecon.ro/colorpicker/
; Used by jquery_colorpicker module.
libraries[colorpicker][download][type] = "git"
libraries[colorpicker][download][url] = "https://git.uwaterloo.ca/libraries/colorpicker.git"
libraries[colorpicker][download][tag] = "2009-05-23"

; iCalcreator
; http://kigkonsult.se/downloads/index.php#iCalcreator
libraries[iCalcreator][download][type] = "git"
libraries[iCalcreator][download][url] = "https://git.uwaterloo.ca/libraries/icalcreator.git"
libraries[iCalcreator][download][tag] = "2.20"
libraries[iCalcreator][directory_name] = "iCalcreator"

; CKEditor Text Selection Plugin
; https://github.com/w8tcha/CKEditor-TextSelection-Plugin/
libraries[textselection][download][type] = "git"
libraries[textselection][download][url] = "https://git.uwaterloo.ca/libraries/CKEditor-TextSelection-Plugin.git"
libraries[textselection][download][branch] = "master"
libraries[textselection][download][revision] = "c439c4e0"
libraries[textselection][directory_name] = "textselection"

; CKEditor CodeMirror Plugin
; https://github.com/w8tcha/CKEditor-CodeMirror-Plugin
libraries[codemirror][download][type] = "git"
libraries[codemirror][download][url] = "https://git.uwaterloo.ca/libraries/CKEditor-CodeMirror-Plugin.git"
libraries[codemirror][download][branch] = "master"
libraries[codemirror][download][revision] = "d1e89d5d"
libraries[codemirror][directory_name] = "codemirror"

; JQuery Cycle
; http://jquery.malsup.com/cycle/
libraries[jquery.cycle][download][type] = "git"
libraries[jquery.cycle][download][url] = "https://git.uwaterloo.ca/libraries/jquery.cycle.git"
libraries[jquery.cycle][download][tag] = "cycle2"

; Modernizr
; http://modernizr.com
libraries[modernizr][download][type] = "git"
libraries[modernizr][download][url] = "https://git.uwaterloo.ca/libraries/modernizr.git"
libraries[modernizr][download][tag] = "2.8.3"

; Tablesaw
; https://github.com/filamentgroup/tablesaw
libraries[tablesaw][download][type] = "git"
libraries[tablesaw][download][url] = "https://git.uwaterloo.ca/libraries/tablesaw.git"
libraries[tablesaw][download][tag] = "v2.0.1"

; Timeline JS
; https://github.com/VeriteCo/TimelineJS
; Base: 2.36.0
; Patch 97daed8: Support @alt for images. https://github.com/NUKnightLab/TimelineJS/pull/818
libraries[timelinejs][download][type] = "git"
libraries[timelinejs][download][url] = "https://git.uwaterloo.ca/libraries/timelinejs.git"
libraries[timelinejs][download][tag] = "2.36.0-uw_wcms1"
libraries[timelinejs][directory_name] = "timeline"

; Underscore.js
; http://underscorejs.org
libraries[underscorejs][download][type] = "git"
libraries[underscorejs][download][url] = "https://git.uwaterloo.ca/libraries/underscorejs.git"
libraries[underscorejs][download][tag] = "1.8.3"
libraries[underscorejs][directory_name] = "underscore"