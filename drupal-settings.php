<?php

// Show all errors on most servers. On live, these are turned off in host_settings.php.
$conf['error_level'] = 2;

foreach (array('db', 'host', 'network', 'cache', 'password') as $file) {
  $file_path = DRUPAL_ROOT . '/config/' . $file. '_settings.php';
  if (file_exists($file_path)) {
    require_once($file_path);
  }
}
unset($file);
unset($file_path);

$update_free_access = FALSE;

if ($UWpref === '') {
  $UW_site_directory = 'uwaterloo.ca';
  $base_url = 'https://' . $UWhost;
}
else {
  $UW_site_directory = 'ca.' . str_replace('/', '.', $UWpref);
  $base_url = 'https://' . $UWhost . '/' . $UWpref;
}

// File system.
$conf['file_default_scheme'] = 'public';
$conf['file_private_path'] = '/wmsfiles/private/' . $UW_site_directory;
$conf['file_public_path'] = 'sites/' . $UW_site_directory . '/files';
$conf['file_temporary_path'] = '/wmsfiles/temp/' . $UW_site_directory;
$conf['file_chmod_directory'] = 02775;
$conf['file_chmod_file'] = 0664;

// Themes.
$conf['admin_theme'] = 'uw_adminimal_theme';
$conf['node_admin_theme'] = '1';
$conf['admin_language_default'] = 'en';

// Set date-related variables.
$conf['date_first_day'] = 0;
$conf['date_api_use_iso8601'] = FALSE;
$conf['date_api_version'] = 7.2;
$conf['date_default_timezone'] = 'America/Toronto';
$conf['user_default_timezone'] = 0;
$conf['date_format_long'] = 'l, F j, Y - H:i';
$conf['date_format_medium'] = 'D, Y-m-d H:i';
$conf['date_format_short'] = 'Y-m-d H:i';
$conf['date_format_event_full'] = 'l, F j, Y - g:ia';
$conf['date_format_event_short'] = 'M j Y - g:ia';
$conf['date_format_long_date_only'] = 'l, F j';
$conf['date_format_medium'] = 'D, Y-m-d H:i';
$conf['date_format_news_short'] = 'M j Y';
$conf['date_format_time_only'] = 'g:ia';

// Disable user picture support and set the default to a square thumbnail option in case it is enabled later.
$conf['user_pictures'] = '0';
$conf['user_picture_dimensions'] = '1024x1024';
$conf['user_picture_file_size'] = '800';
$conf['user_picture_style'] = 'thumbnail';

// Other.
$conf['shortcut_max_slots'] = 12;
$conf['user_register'] = 0; // Same value as constant USER_REGISTER_ADMINISTRATORS_ONLY. Constant not yet defined.
$conf['scheduler_publisher_user'] = 'specific_user';
$conf['scheduler_date_popup_minute_increment'] = 15;
$conf['update_check_disabled'] = TRUE;
$conf['update_notify_emails'] = NULL;
$conf['cas_domain'] = 'uwaterloo.ca';
$conf['site_403'] = '';
$conf['site_404'] = '';
$conf['site_default_country'] = 'CA';
$conf['site_mail'] = 'wcmsadmin@uwaterloo.ca';
$conf['syslog_facility'] = 160;
$conf['syslog_format'] = '!base_url|!timestamp|!type|!ip|!request_uri|!referer|!uid|!link|!message';
$conf['syslog_identity'] = 'drupal';

// XPath query that defines which tables will be made responsive by the
// responsive_tables_filter module.
$conf['responsive_tables_filter_table_xpath'] = "//table[not(contains(concat(' ',normalize-space(@class),' '),' no-responsive '))]";

$databases = array();
// Primary database server.
$databases['default']['default'] = array(
  'driver' => 'mysql',
  'database' => $UWdb,
  'username' => $UWuser,
  'password' => $UWpass,
  'host' => $UWpri,
  'port' => '',
  'prefix' => '',
);
// Secondary database server, if configured.
if (isset($UWsec)) {
  $databases['default']['slave'] = array(
    'driver' => 'mysql',
    'database' => $UWdb,
    'username' => $UWuser,
    'password' => $UWpass,
    'host' => $UWsec,
    'port' => '',
    'prefix' => '',
  );
}
